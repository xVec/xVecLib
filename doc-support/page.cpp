
/**
 *
 */
/**
 * @page Guides
 * @section Guides PID and Related Guides
 * @subsection PID
 * @par An overview on PID.
 * PID uses 3 values P, I, and D
 * Stands for Integral Porpotional, and Derivative
 * @subsection Odometry
 * @par Odometry
 * [https://github.com/team914/autolib-pdfs/blob/master/adaptive-pure-pursuit.pdf] \n
 * Odometry is finding the robot’s XY location. \n
 * The simplest way of doing this is to use encoders attached to the drive wheels to measure how far the robot moved, and to use the gyro to measure in which direction. \n
 * By repeating this math 50 times per second, the robot gets a fair    approximation of its current location. \n
 * The math is as follows: \n
 * \code
 * distance = (change in left encoder value + change in right encoder value)/2
 * x location += distance * cosine(robot  angle)
 * y location += distance * sine(robot  angle)
 * \endcode
 * This gives the location of the robot relative to its starting location, which by default is (0, 0). \n
 * If you change the starting location to be the robot’s starting location on the field, then the calculated location will be field-centric (relative to the field).
 * When running paths one after another, using field-centric coordinates can reduce buildup of errors caused by the robot not stopping precisely , as the robot will still know where it is on the field and can correct itself during the next path.
 * Field-centric coordinates can also make drawing paths more intuitive.
 * You could also use other forms of odometry, such as vision or LIDAR
 *
 */
/**
 * @page Guides
 * @section myOdom My Odometry
 * @attention My odometry, invoked by updateOdom, most likely wouldn't be considered the best approach for odometry. \n
 * @par Normal Odometry tracks on the x axis, and uses cosine(theta) for tracking x and sine(theta) for y.
 * Due to the lack of external encoders on our original robot, the distance traveled is tracked using the motor encoder values of 1 motor on each side. \n
 * This can pose as a bug if you are expecting to move sideways in any way (The robot can't track movement if the robot isn't facing that axis). If not, it tracks very consistently. Code has been written to try and fix that, but it's unfinished. It will be finished for version 1.4.0 \n
 * @note The following are planed out for future releases:
 * @li  Add a way to change how the inertial sensor tracks
 * @li Add support for different types of input sensors and different positions
 * @li Finish adding support for axis changing
 *
 */
/**
 * @page Guides
 * @section moveTo The moveTo method
 * @attention This is the most developed of all the methods in this library.
 * @par The moveTo method turns and moves the robot to the specified location (x, y) cordinates (in inches). 
 * This is relative to the robot's starting position, but it can become field-centric if the robot's position and rotation is set to the robot's one.
 * 
 *
 */
/**
 * @file page.cpp
 * @brief Contains some additional docummentation
 */