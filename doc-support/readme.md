
# Welcome to xVecLib

\note This Library is in pre-release! Be prepared for bugs and please report them on the [repository bug page](https://codeberg.org/xvec/xveclib/issues/)

Here is the link to the in-depth API [documentation](https://xvec.codeberg.page)
	xVecLib also supports Path Generation. 
## Installation

Currently, there's no automated installation method. In order to install, you must drag and drop the include/xVecLib and the src/xVecLib folders to your project's include and src folders. Then include it from your main file like so.

```cpp
#include "xVecLib/xVecLib.hpp"
```

## Setup

In order to setup, you must give references to your robot. Currently, the library requires just an inertial sensor.
Here is an example created in PROS:

```cpp
pidSettings
    linearController(5,  // P
                     0,  // I
                     10, // D
                     3,  // Max I
                     1,  // small error range, in inches
                     3,  // large error range, in inches
                     5   // maximum acceleration (slew)
    );

pidSettings
    angularController(exactTP, // proportional gain (kP)
                      0,       // integral gain (kI)
                      6,       // derivative gain (kD)
                      3,       // anti windup
                      1.7,     // small error range, in degrees
                      4,       // large error range, in degrees
                      5        //(slew)
		    );

pros::MotorGroup left_mg({1, 2, 3});
pros::MotorGroup right_mg({4, 5, 6});
pros::IMU imu = pros::IMU(7);
double wheelDiameter = 3.5;
double gearRatio = 2.0;
robot hs = robot(&right_mg, &left_mg, &imu, linearController, angularController,
                 wheelDiameter, gearRatio);

```

If your robot already has an inertial sensor, you can start using this library.

### Usage

This Library provides several different functions that can be used by less experienced users.
Currently, the moveTo functions are still being tested and may contain bugs. Please report any bugs to the [repository bug tracker](https://codeberg.org/xvec/xveclib/issues/).

\attention PID Setting
Please make sure to set your PID values using either the robot constructor or the corresponding setters. xVecLib::PID::setDrivePID xVecLib::PID::setTurnPID


It's also recommended to call updateOdom() during your user control.

The following is a list of functions available with other useful information.


| Function Name | Complexity   | Control Structure | Link                                                                                                                                   | Stability      |
| ------------- | ------------ | ----------------- | -------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| DrivePID      | Beginner     | PID               | [Link](https://xvec.codeberg.page/class_x_y_zv_lib_1_1_p_i_d_a99aaee9344652e6e78f86ae0bacbecd8.html#a99aaee9344652e6e78f86ae0bacbecd8) | Stable         |
| turnPID       | Beginner     | PID               | [Link](https://xvec.codeberg.page/class_x_y_zv_lib_1_1_p_i_d_a2c9933c7ba4fe52f66a13da3d6e74c3f.html#a2c9933c7ba4fe52f66a13da3d6e74c3f) | Stable         |
| turnTo        | Beginner     | PID               | [Link](https://xvec.codeberg.page/class_x_y_zv_lib_1_1_p_i_d_a65fe08b95b8ef5003bd3f2b8445026bf.html#a65fe08b95b8ef5003bd3f2b8445026bf) | Stable         |
| right_swing   | Intermediate | PID               | [Link](https://xvec.codeberg.page/class_x_y_zv_lib_1_1_p_i_d_a42d7ba2fae8d8af829259b891596aea9.html#a42d7ba2fae8d8af829259b891596aea9) | Untested       |
| left_swing    | Intermediate | PID               | [Link](https://xvec.codeberg.page/class_x_y_zv_lib_1_1_p_i_d_ad6b96534b2758efa63a90fd527fe4967.html#ad6b96534b2758efa63a90fd527fe4967) | Untested       |
| moveTo        | Advanced     | PID with Odometry | [Link](https://xvec.codeberg.page/class_x_y_zv_lib_1_1_p_i_d_a9f795fbd9d58b18905037eb86c8bab08.html#a9f795fbd9d58b18905037eb86c8bab08) | Potential Bugs |
| moveToBoom    | Advanced     | Boomerang         | [Link](https://xvec.codeberg.page/class_x_y_zv_lib_1_1_p_i_d_ab96f6a3edb6bccf97d240b3154b0110b.html#ab96f6a3edb6bccf97d240b3154b0110b) | Some bugs      |
[Guides](https://xvec.codeberg.page/_guides.html) to some of the control structures.

[Link](https://github.com/team914/autolib-pdfs) to a collection of useful PDF for more in-depth description.

[Ramsete](https://wiki.purduesigbots.com/software/control-algorithms/ramsete) and [Boomerang](https://area-53-robotics.github.io/Intro-To-Robotics/software/advanced-concepts/boomerang/) description.

Graphs: [Boomerang](https://www.desmos.com/calculator/7f8bt5csk8) [moveToSpeed](https://www.desmos.com/calculator/1zbdbnjqgd) [Pilons Input Scaling](https://www.desmos.com/calculator/l1bog8xmw4).


		
