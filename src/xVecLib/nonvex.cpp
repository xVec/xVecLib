
/**
 * @file nonvex.cpp
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief Various non-vex code
 * @version 0.1.0
 * @date 2024-01-10
 *
 * @copyright Copyright Cavaire(c) 2024 with code from LemLib
 *
 */

#include <vector>
#include "xVecLib/nonvex.hpp"

//#include "xVecLib/draw.hpp"
#include <cmath>

namespace xVecLib
{
    /**
     * @brief Add a Point to this Point
     *
     * @param other other Point
     * @return Point
     */
    Point Point::operator+(const Point &other)
    {
        return Point(this->x + other.x, this->y + other.y, this->theta);
    }

    /**
     * @brief Subtract a Point from this Point
     *
     * @param other other Point
     * @return Point
     */
    Point Point::operator-(const Point &other)
    {
        return Point(this->x - other.x, this->y - other.y, this->theta);
    }

    /**
     * @brief Multiply a Point by this Point
     *
     * @param other other Point
     * @return Point
     */
    double Point::operator*(const Point &other) { return this->x * other.x + this->y * other.y; }

    /**
     * @brief Multiply a Point by a double
     *
     * @param other double
     * @return Point
     */
    Point Point::operator*(const double &other)
    {
        return Point(this->x * other, this->y * other, this->theta);
    }

    /**
     * @brief Divide a Point by a double
     *
     * @param other double
     * @return Point
     */
    Point Point::operator/(const double &other)
    {
        return Point(this->x / other, this->y / other, this->theta);
    }

    /**
     * @brief Linearly interpolate between two Points
     *
     * @param other the other Point
     * @param t t value
     * @return Point
     */
    Point Point::lerp(Point other, double t)
    {
        return Point(this->x + (other.x - this->x) * t, this->y + (other.y - this->y) * t, this->theta);
    }

    /**
     * @brief Get the distance between two Points
     *
     * @param other the other Point
     * @return double
     */
    double Point::distance(Point other) const { return hypot(this->x - other.x, this->y - other.y); }

    /**
     * @brief Get the angle between two Points
     *
     * @param other the other Point
     * @return double in radians
     */
    double Point::angle(Point other) const { return std::atan2(other.y - this->y, other.x - this->x); }

    /**
     * @brief Rotate a Point by an angle
     *
     * @param angle angle in radians
     * @return Point
     */
    Point Point::rotate(double angle)
    {
        return Point(this->x * cos(angle) - this->y * sin(angle),
                     this->x * sin(angle) + this->y * cos(angle), this->theta);
    }
    double convertRadians(double num) { return num * (M_PI / 180); }
    double convertDegrees(double num) { return num * (180 / M_PI); }
    double calcInchesPerTick(double current, double size, double ratio) { return ((current * ((size * (ratio)) * M_PI)) / 360); }
    double getTilePos(double pos) { return fabs(pos / 24); }
    double getPosTile(double tile) { return tile * 24; }
    double checkSign(double num) { return num > 0 ? 1 : num < 0 ? -1
                                                                : 0; }
    /**
     * @brief Slew rate limiter
     *
     * @param target target value
     * @param current current value
     * @param maxChange maximum change. No maximum if set to 0
     * @return double - the limited value
     */
    double slew(double target, double current, double maxChange)
    {
        double change = target - current;
        if (maxChange == 0)
            return target;
        if (change > maxChange)
            change = maxChange;
        else if (change < -maxChange)
            change = -maxChange;
        return current + change;
    }

    /**
     * @brief Calculate the error between 2 angles. Useful when calculating the error between 2 headings
     *
     * @param angle1
     * @param angle2
     * @param radians true if angle is in radians, false if not. False by default
     * @return double wrapped angle
     */
    double angleError(double angle1, double angle2, bool radians)
    {
        return std::remainder(angle1 - angle2, radians ? 2 * M_PI : 360);
    }

    /**
     * @brief Return the average of a vector of numbers
     *
     * @param values
     * @return double
     */
    double avg(std::vector<double> values)
    {
        double sum = 0;
        for (double value : values)
        {
            sum += value;
        }
        return sum / values.size();
    }

    /**
     * @brief Exponential moving average
     *
     * @param current current measurement
     * @param previous previous output
     * @param smooth smoothing factor (0-1). 1 means no smoothing, 0 means no change
     * @return double - the smoothed output
     */
    double ema(double current, double previous, double smooth)
    {
        return (current * smooth) + (previous * (1 - smooth));
    }

    /**
     * @brief Get the signed curvature of a circle that intersects the first Point and the second Point
     *
     * @note The circle will be tangent to the theta value of the first Point
     * @note The curvature is signed. Positive curvature means the circle is going clockwise, negative means
     * counter-clockwise
     * @note Theta has to be in radians and in standard form. That means 0 is right and increases counter-clockwise
     *
     * @param first the first Point
     * @param other the second Point
     * @return double curvature
     */
    double getCurvature(Point first, Point other)
    {
        // calculate whether the Point is on the left or right side of the circle
        double side = sgn(std::sin(first.theta) * (other.x - first.x) - std::cos(first.theta) * (other.y - first.y));
        // calculate center point and radius
        double a = -std::tan(first.theta);
        double c = std::tan(first.theta) * first.x - first.y;
        double x = std::fabs(a * other.x + other.y + c) / std::sqrt((a * a) + 1);
        double d = std::hypot(other.x - first.x, other.y - first.y);

        // return curvature
        return side * ((2 * x) / (d * d));
    }
}