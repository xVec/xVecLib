  /**
 * @file draw.cpp
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief Drawing class that draws data on the screen
 * @version 0.2.0
 * @date 2024-01-10
 *
 *
 */
#include "xVecLib/draw.hpp"
#include "xVecLib/xVecLib.hpp"
#define fieldscale 1.66548042705

namespace xVecLib {

void drawing::drawTile(double x, double y) {
  // lv_draw_rect(0,0,{245 + ((x - 1) * 39), 245 + ((x - 1) * 39)+39,
  std::uint32_t col = xapi::get_pen();
  xapi::fill_rect(245 + ((x - 1) * 39) + 1, 2 + (39 * (y - 1)) + 1,
                          245 + ((x - 1) * 39) + 39 - 1,
                          2 + (39 * (y - 1)) + 39 - 1);
  xapi::set_pen(0xFFFFFF);
  xapi::draw_rect(245 + ((x - 1) * 39), 2 + (39 * (y - 1)),
                          245 + ((x - 1) * 39) + 39, 2 + (39 * (y - 1)) + 39);
  xapi::set_pen(col);
}

void drawing::drawField() {
  // Brain.Screen.drawImageFromBuffer(vexEmptyAuto, 0, 0,
  // sizeof(vexEmptyAuto));
  xapi::set_pen(0x313335);
  for (size_t i = 0; i <= 5; i++) {
    for (size_t j = 1; j <= 6; j++) {
      drawTile(i, j);
    }
  }
  xapi::set_pen(0xD21F3C);

  drawTile(0, 1);
  drawTile(1, 1);
  drawTile(2, 1);
  drawTile(3, 1);
  drawTile(4, 1);
  drawTile(5, 1);

  xapi::set_pen(0x0E4D92);
  drawTile(0, 6);
  drawTile(1, 6);
  drawTile(2, 6);
  drawTile(3, 6);
  drawTile(4, 6);
  drawTile(5, 6);
}

void drawing::xVecLibScreen() {

  // buttonRect(10, 40, -20 + textadjustvalue, 0 + textadjustvalue, black,
  // white, auton1Name, 1, 6, 3, 0, 1);
  //  buttonRect(10, 40, 40 + textadjustvalue, 60 + textadjustvalue, black,
  //  white, auton2Name, 1, 6, 3, 0, 2); buttonRect(10, 40, 100 +
  //  textadjustvalue, 120 + textadjustvalue, black, white, auton3Name, 1, 6,
  //  3, 0, 3); buttonRect(10, 40, 150 + textadjustvalue, 170 +
  //  textadjustvalue, black, white, auton4Name, 1, 6, 3, 0, 4);

  // Brain.Screen.setPenColor(vex::color(255, 255, 255));
  // Brain.Screen.setFillColor(vex::color(0, 0, 0));

  // This draws the robot body with position and angle
  double yfieldvalue = ((-ro.robotY - 72) * fieldscale) + 245 - 10;
  double xfieldvalue = ((ro.robotX + 72) * fieldscale) + 245 - 39;
  // moving locations
  //  max y value 5.82914886039(tiles) = y value 2
  //  min y value 0 = y value 235 (out of 239)
  // min y value not in range, min 1 = 195.028469751
  // need real y value 0,0 in middle.
  //  0 y must equal tile 3.5 instead of
  //  0 y equals edge of tile max
  //  must add 3.5 tiles up
  // 3.5*24=84 (inches)

  if (xfieldvalue != prevX || yfieldvalue != prevY) {
    drawField();
  }
  xapi::set_pen(0xF0F1F1);
  xapi::fill_circle(xfieldvalue, yfieldvalue, 10);
  xapi::set_pen(0x313335);

  xapi::draw_circle(xfieldvalue, yfieldvalue, 10);
  xapi::draw_line(xfieldvalue, yfieldvalue,
                          xfieldvalue + cos(ro.Theta - (M_PI / 2)) * 15,
                            yfieldvalue + sin(ro.Theta - (M_PI / 2)) * 15);
  prevX = xfieldvalue;
  prevY = yfieldvalue;
}
}; // namespace xVecLib
