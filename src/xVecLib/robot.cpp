/**
 * @file robot.cpp
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief Contains the code for the robot class
 * @version 0.2.0
 * @date 2024-01-10
 *
 * @copyright Copyright (c) 2024
 *
 */
#include "xVecLib/robot.hpp"
#include "xVecLib/xVecLib.hpp"
#include "xVecLib/control.hpp"
#include "xVecLib/provex.hpp"



namespace xVecLib
{

  robot::robot(xapi::MotorGroup *_rightMotors, xapi::MotorGroup *_leftMotors, xapi::IMU *_imu, pidSettings linearSetting,
               pidSettings angularSetting, double wheelDiameter, double gearRatio)
      : control::control(_rightMotors, _leftMotors, _imu, linearSetting, angularSetting, wheelDiameter, gearRatio) {};
  void robot::driveForwardTiles(double tiles)
  {
    // DriveFront((inchesPerGrid * tiles)/2,0);
    drivePID(inchesPerGrid * tiles);
    // pros::delay(200);
  }

  void robot::driveForwardInches(double inches) { DriveFront(inches, 0); }

  void robot::driveReverseTiles(double tiles)
  {
    drivePID(-(inchesPerGrid * tiles));
    // pros::delay(200);
  }

  void robot::driveReverseInches(double inches) { DriveBack(inches, 0); }

  void robot::turnRight(double degree) { turnPID((-degree)); }
  void robot::turnLeft(double degree) { turnPID(degree); }

  // 3 3 = 60,60
  void robot::moveToTile(double tileX, double tileY)
  {
    double x = (tileX * 24) - 12;
    double y = (tileY * 24) - 12;
    moveTo(x, y);
  }

  void robot::addTilePoint(Point tile)
  {
    addPoint({(tile.getX() * 24) - 12, (tile.getY() * 24) - 12});
  }
  void robot::addTilePoint(double tileX, double tileY)
  {
    addPoint({(tileX * 24) - 12, (tileY * 24) - 12});
  }
  void robot::addFieldPoint(double tileY, double tileX)
  {
    addPoint({(tileX * 24) - 12, (tileY * 24) - 12});
  }
} // namespace xVecLib