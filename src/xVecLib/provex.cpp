/**
 * @file provex.cpp
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief
 * @version 1.0.0
 * @date 2024-10-22
 *
 * @copyright Copyright (c) 2024
 *
 */
#include "universal.h"
#include "xVecLib/provex.hpp"
#ifdef VEX
/**
 * @brief XAPI for vex namespace
 * 
 */
namespace xapi {

    
}
#endif

#ifdef PRO
#include "pros/screen.hpp"
#include "pros/rtos.hpp"

/**
 * @brief  XAPI for pros namespace
 */
namespace xapi {
    
    std::uint32_t get_pen(){ return pros::screen::get_pen(); };
    std::uint32_t fill_rect(const std::int16_t x0, const std::int16_t y0, const std::int16_t x1, const std::int16_t y1){ return pros::screen::fill_rect(x0, y0, x1, y1); };
    std::uint32_t set_pen(std::uint32_t color){ return pros::screen::set_pen(color); };
    std::uint32_t draw_rect(const std::int16_t x0, const std::int16_t y0, const std::int16_t x1, const std::int16_t y1){ return  pros::screen::draw_rect(x0, y0, x1, y1); };
    std::uint32_t fill_circle(const std::int16_t x, const std::int16_t y, const std::int16_t radius) { return  pros::screen::fill_circle(x, y, radius); };
    std::uint32_t draw_circle(const std::int16_t x, const std::int16_t y, const std::int16_t radius) { return  pros::screen::draw_circle(x, y, radius); };
    std::uint32_t draw_line(const std::int16_t x0, const std::int16_t y0, const std::int16_t x1, const std::int16_t y1) { return  pros::screen::draw_line(x0, y0, x1, y1); };
    uint32_t millis(void) { return pros::millis(); };               
    void delay(const uint32_t milliseconds) { pros::delay(milliseconds); };
    
}
#endif
