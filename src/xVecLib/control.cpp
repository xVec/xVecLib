/**
 * @file control.cpp
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief The code for the control class
 * @version 0.9.5
 * @date 2024-05-13
 * @copyright Copyright (c) 2024
 */

#include "xVecLib/control.hpp"

#include "xVecLib/nonvex.hpp"
#include "xVecLib/provex.hpp"
#include <cmath>
#include <cstdio>
#include <math.h>
#include <vector>

#include "pros/rtos.hpp"
#include "xVecLib/xVecLib.hpp"
#include <numeric>
#include <vector>

namespace xVecLib
{

  control::control(xapi::MotorGroup *_rightMotors, xapi::MotorGroup *_leftMotors,
                   xapi::IMU *_imu, pidSettings lateralSettings,
                   pidSettings angularSettings, double wheelDiameter,
                   double gearRatio)
      : rightMotors(_rightMotors), leftMotors(_leftMotors), imu(_imu),
        drivePIDv(lateralSettings.kP, lateralSettings.kI, lateralSettings.kD,
                  lateralSettings.windupRange, true),
        turnPIDv(angularSettings.kP, angularSettings.kI, angularSettings.kD,
                 angularSettings.windupRange, true),
        lateralSettings(lateralSettings), angularSettings(angularSettings), wheelDiameter(wheelDiameter), gearRatio(gearRatio) {};

  double average(std::vector<double> const &v)
  {
    if (v.empty())
    {
      return 0;
    }

    auto const count = static_cast<float>(v.size());
    return std::reduce(v.begin(), v.end()) / count;
  }

  bool whatAxis(cAxis ax)
  {
    if (ax == y)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  bool control::isYaxis()
  {
    if (ro.robotAxis == y)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  void control::setOdomAngle(cAxis odomAxis)
  {
    ro.robotAxis = odomAxis;
    if (odomAxis == x)
    {
      ro.robotAngle = -90;
      imu->set_rotation(-90);
    }
  }
  void control::addOdom(odom *newOdom)
  {
    additionOdom = newOdom;
  }
  void control::updateOdom(void)
  {
    odomIter++;
    /*Odom*/

    currentHead = imu->get_rotation();
    currentHead = fmod(currentHead, 360);
    if (!std::isfinite(ro.robotX) || !std::isfinite(ro.robotY))
    {
      ro.robotX = 0.0000001;
      ro.robotY = 0.0000001;
      ro.Theta = 0.00000001;
      std::cout << "\033[2J\033[1;1H"; // clear the terminal
    }
    if (additionOdom != nullptr)
    {
      currentL = additionOdom->getDistanceTraveled();
      currentR = additionOdom->getDistanceTraveled();
    }
    else
    {
      currentL = leftMotors->get_position(0);
      currentR = rightMotors->get_position(0);
      if (!std::isfinite(wheelDiameter) || !std::isfinite(gearRatio) ||
          gearRatio == 0 || wheelDiameter == 0)
      {
        wheelDiameter = size;
        gearRatio = gear;
        std::cout << "\033[2J\033[1;1H"; // clear the terminal
        printf("Wheel Diameter %f, Gear Ratio %f", wheelDiameter, gearRatio);
      }
      deltaL = calcInchesPerTick(currentL - previousL, wheelDiameter, gearRatio);
      deltaR = calcInchesPerTick(currentR - previousR, wheelDiameter, gearRatio);
      deltaD = (deltaL + deltaR) / 2;
    }
    deltaTheta = convertRadians(currentHead - previousHead);
    if (deltaTheta != 0)
    {
      ro.robotX += deltaD * sin(ro.Theta);
      ro.robotY += deltaD * cos(ro.Theta);
      // cos(0)= 1 * dis = dis - Explanation for why y is positive
    }
    else
    {
      ro.robotX += deltaD;
      ro.robotY += deltaD;
    }
    // Posiible fixes:
    //  cos(90) = 0 cos(-90)= 0 - no
    // In order to switch them around(so x is naturaly negative), the robot must
    // be placed looking at the x axis positive
    //  cos(0) = 1  sin(90)=1
    ro.Theta += deltaTheta;
    ro.robotAngle = currentHead;
    previousL = currentL;
    previousR = currentR;
    previousHead = currentHead;
    deltaTheta = 0;
    drawer.xVecLibScreen();
    // checkOverheat();
    if (odomIter % 4 == 0)
    {
      moveCursor(1, 16); // move the cursor to position (1, 16)
      std::cout << "RobotX: " << ro.robotX << " RobotY: " << ro.robotY
                << " RobotAngle: " << ro.robotAngle
                << std::endl; // print the new position
    }
    else if (odomIter >= imax)
    {
      odomIter = 0;
    }

    xapi::delay(10);
  }
  void control::moveCursor(int x, int y)
  {
    std::cout << "\033[" << y << ";" << x << "H";
  }
  void control::drivePID(double target, double limit)
  {
    double Error;
    UseDrivePID = true;
    double DrivePower = 0;
    drivePIDv.reset();
    leftMotors->set_zero_position(0);
    rightMotors->set_zero_position(0);
    double maxTimePerTile = 2; // 0.36
    double positionAverage = 0;
    int pidIter = 0;
    xapi::delay(100);
    if (limit == 0)
      limit=3000;  
    // limit = fabs(maxTimePerTile * (target / 24));

    double prevDrive = 0;
    double currentTime = xapi::millis() + limit;
    move.take(limit);
    while (UseDrivePID)
    {
      pidIter++;
      updateOdom();
      double currentL = leftMotors->get_position();
      double currentR = rightMotors->get_position();
      positionAverage =
          calcInchesPerTick((currentL + currentR) / 2, wheelDiameter, gearRatio);
      Error = target - positionAverage;

      // Error = checkSign(target) != 1 ? Target - positionAverage :
      // (std::abs(Target) - std::abs(positionAverage)) * -1;
      DrivePower = drivePIDv.update(Error);
      if (xapi::millis() > currentTime)
      {
        UseDrivePID = false;
        break;
        /* code */
      }
      
      if (pidIter % 20 == 0)
      {
        // printf("\n Travel: %f\n Timer Value %f\n Limit: %d", limit,
        //        positionAverage, Ti.getTimeLeft());
        printf("\n Error: %f", Error);
        printf("\n DrivePower: %f", DrivePower);
      }
      else if (pidIter > imax)
      {
        pidIter = 0;
      }

      if (Error <= lateralSettings.largeError &&
          Error >= -lateralSettings.largeError)
      {
        rightMotors->brake();
        leftMotors->brake();
        printf("\n Drive Ended with low error");
        UseDrivePID = false;
      }
      else
      {
        rightMotors->move(DrivePower);
        leftMotors->move(DrivePower);
        prevDrive = DrivePower;
      }
      xapi::delay(10);
    }
    rightMotors->brake();
    leftMotors->brake();
    move.give();
  }
  void control::drivePID(double target, bool fixAngle)
  {
    double prevAngle = ro.robotAngle;
    drivePID(target);
    if (prevAngle != ro.robotAngle)
    {
      xapi::delay(150);
      double dif = prevAngle - ro.robotAngle;
      turnPID(dif);
    }
  }

  void control::turnPID(double tTarget)
  {
    move.take(1);
    double TurnTarget;
    TurnTarget = tTarget + ro.robotAngle;
    UseTurnPID = true;
    while (UseTurnPID)
    {
      updateOdom();
      double turnError = (TurnTarget)-ro.robotAngle;
      power = turnPIDv.update(turnError);
      if ((turnError <= angularSettings.smallError &&
           turnError >= -angularSettings.smallError))
      {
        rightMotors->brake();
        leftMotors->brake();
        UseTurnPID = false;
        printf("\n Turn Ended with low error");
      }
      else
      {
        rightMotors->move(power);
        leftMotors->move(-power);
      }
      xapi::delay(10);
    }
    move.give();
  }

  void control::turnTo(double targetRotation)
  {
    move.take(1000);
    int pidIter = 4;
    UseTurnPID = true;
    power = 0;
    turnPIDv.reset();
    double initialTurnError = targetRotation - ro.robotAngle;
    double currentTime = xapi::millis() + (750 * (fabs(initialTurnError) / 90));
    moveCursor(1, 2); // move the cursor to position (1, 16)

    std::cout << "Target Rotation for TurnTo: " << targetRotation
              << "\t Time Left: " << currentTime - xapi::millis() << "ms"
              << std::endl;

    double power = 0;
    bool turnLeft = (ro.robotAngle + 10) - targetRotation <
                    (ro.robotAngle - 10) - targetRotation;
    while (UseTurnPID && (xapi::millis() < currentTime))
    {
      pidIter++;
      updateOdom();

      double turnError = targetRotation - fmod(ro.robotAngle, 360);
      power = turnPIDv.update(turnError);
      if (pidIter % 5 == 0)
      {
        moveCursor(1, 3); // move the cursor to position (1, 16)
        std::cout << "turnError: " << turnError << std::endl
                  << "power: " << power << std::endl
                  << "Rotation: " << ro.robotAngle << std::endl;
      }
      else if (pidIter > imax)
      {
        pidIter = 0;
      }
      double low = angularSettings.smallError > 1 ? angularSettings.smallError : 1;
      if (abs(turnError) <= low && -abs(turnError) >= -low)
      {
        rightMotors->brake();
        leftMotors->brake();
        UseTurnPID = false;
        moveCursor(1, 5); // move the cursor to position (1, 16)
        std::cout << "TurnTo " << targetRotation << " Ended with low error of " << turnError << " out of " << low << std::endl;
        move.give();
      }
      else
      {
        if (turnLeft)
        {
          rightMotors->move(-power);
          leftMotors->move(power);
        }
        else
        {
          rightMotors->move(power);
          leftMotors->move(-power);
        }
      }

      xapi::delay(10);
    }
    move.give();
  }

  void control::setDrivePID(pidValue pidVal) { drivePIDv = pidVal; }

  pidValue control::getDrivePID() { return drivePIDv; }

  void control::setTurnPID(pidValue pidVal) { turnPIDv = pidVal; }

  pidValue control::getTurnPID() { return turnPIDv; }

  void control::setHelperPID(pidValue pidVal) { turnPIDv = pidVal; }

  pidValue control::getHelperPID() { return turnPIDv; }

  void control::DriveFront(double DriveTargetX, double DriveTargetY)
  {
    drivePID(sqrt((double)pow((DriveTargetX - ro.robotX), 2) +
                  (double)pow((DriveTargetY - ro.robotY), 2)));
  }

  void control::DriveBack(double DriveTargetX, double DriveTargetY)
  {
    drivePID(-1 * (sqrt((double)pow((DriveTargetX - ro.robotX), 2) +
                        (double)pow((DriveTargetY - ro.robotY), 2))));
  }

  void control::ramsete(double X, double Y)
  {
    bool StartHelperPID = false;
    double errorX, errorY, ErrorAngle;
    double currentX, currentY;
    double K, helperP, helperD, LineValue, AngleValue;
    double MakeDown, Linear, Angular;
    double LinearMotor, AngularMotor, WheelCir = 3.25;
    double LeftPower, RightPower;
    double TargetX = X;
    double TargetY = Y;
    double TargetAngle = -convertDegrees(atan2(X - ro.robotX, Y - ro.robotY));
    bool StartRamsete = false;
    double ErrorX, ErrorY;
    double GlobalX, GlobalY, GlobalAngle;
    double kB, kC, LinVel, AngVel;
    double DownScaler;
    StartRamsete = true;
    kB = 1;
    /* proportional term for the controller.  kB > 0. */
    kC = 1;
    /* damping term - between 0 and 1. */
    while (StartRamsete)
    {
      GlobalX = ro.robotX;
      GlobalY = ro.robotY;
      GlobalAngle = ro.robotAngle;

      ErrorX = (cos(GlobalAngle) * (TargetX - GlobalX)) +
               (sin(GlobalAngle) * (TargetY - GlobalY));
      ErrorY = (cos(GlobalAngle) * (TargetY - GlobalY)) +
               ((sin(GlobalAngle) * -1) * (TargetX - GlobalX));
      printf("ErrorX: %f\n", ErrorX);
      printf("ErrorY: %f\n", ErrorY);
      ErrorAngle = TargetAngle - convertDegrees(GlobalAngle);
      printf("ErrorAngle: %f\n", ErrorAngle);
      DownScaler = .01;
      LinVel = ErrorX * DownScaler;
      AngVel = ErrorAngle * DownScaler;

      K = 2 * kC * sqrt((pow(AngVel, 2) + kB) * pow(LinVel, 2));

      Linear = LinVel * cos(ErrorAngle) + K * ErrorX;
      Angular = AngVel + K * ErrorAngle +
                ((kB * LinVel * sin(ErrorAngle) * ErrorY) / ErrorAngle);
      printf("Linear: %f\n", Linear);
      printf("Angular: %f\n", Angular);
      LinearMotor = Linear / WheelCir;
      AngularMotor = Angular / WheelCir;

      LeftPower = LinearMotor + AngularMotor;
      RightPower = LinearMotor - AngularMotor;
      printf("LeftPower: %f\n", LeftPower);

      printf("RightPower: %f\n", RightPower);

      if (ErrorX < 2 && ErrorX > -2 && ErrorY < 2 && ErrorY > -2 &&
          ErrorAngle < 5 && ErrorAngle > -5)
      {
        rightMotors->brake();
        leftMotors->brake();
        StartRamsete = false;
        //
      }
      else
      {
        rightMotors->move(RightPower);
        leftMotors->move(LeftPower);
      }
      xapi::delay(10);
      updateOdom();
    }
  }

  void control::clearPath(void) { movePointArray.clear(); }

  void control::addPoint(Point NewPoint) { movePointArray.push_back(NewPoint); }
  void control::curvePID(double targetX, double targetY, bool goingForward, bool slowdown)
  {

    updateOdom();
    // PID constants
    drivePIDv.reset();
    turnPIDv.reset();
    int pidIter = 0;
    double kpDistance = drivePIDv.P; // Proportional constant for distance
    double kdDistance = drivePIDv.D; // Derivative constant for distance
    double kpHeading = turnPIDv.P;   // Proportional constant for heading
    double kdHeading = turnPIDv.D;   // Derivative constant for heading

    // Distance PID variables
    double prevDistanceError = 0;

    // Heading PID variables
    double headingError = 0, prevHeadingError = 0;

    // Maximum motor speeds
    // const double maxSpeed = 100;
    double errorX = targetX - ro.robotX;
    double errorY = targetY - ro.robotY;
    double distanceError = sqrt(errorX * errorX + errorY * errorY);
    double MaxdistanceError = sqrt(errorX * errorX + errorY * errorY);
    double targetHeading = atan2(errorX, errorY) * 180.0 / M_PI;
    if (!goingForward)
    {
      targetHeading = fmod(targetHeading + 180,
                           360); // Flip heading by 180 degrees to move backward
    }
    move.take(MaxdistanceError / 6);
    // Main loop
    while (true)
    {
      pidIter++;
      // Update odometry
      updateOdom();
      // Calculate distance error
      errorX = targetX - ro.robotX;
      errorY = targetY - ro.robotY;
      targetHeading = atan2(errorX, errorY) * 180.0 / M_PI;
      double currentHeading = fmod(imu->get_rotation(), 360.0);
      distanceError = sqrt(errorX * errorX + errorY * errorY);
      if (targetHeading < 0)
      {
        targetHeading += 360;
      }
      if (!goingForward)
      {
        targetHeading = fmod(targetHeading + 180,
                             360); // Flip heading by 180 degrees to move backward
      }
      // Calculate heading error

      headingError = targetHeading - currentHeading;

      // Adjust for shortest turn
      if (headingError > 180)
      {
        headingError -= 360;
      }
      else if (headingError < -180)
      {
        headingError += 360;
      }

      // PID control for distance
      double distancePower = kpDistance * MaxdistanceError +
                             kdDistance * (distanceError - prevDistanceError);

      if (slowdown)
      {
        distancePower = kpDistance * distanceError +
                        kdDistance * (distanceError - prevDistanceError);
      }

      // PID control for heading
      double headingPower = kpHeading * headingError +
                            kdHeading * (headingError - prevHeadingError);
      if (pidIter % 5 == 0)
      {
        // printf("\n Travel: %f\n Timer Value %f\n Limit: %d", limit,
        //        positionAverage, Ti.getTimeLeft());
        printf("\n Error: %f", headingError);
        printf("\n DrivePower: %f", headingPower);
      }
      else if (pidIter > imax)
      {
        pidIter = 0;
      }
      // Apply motor speeds
      if (!goingForward)
      {
        distancePower = -distancePower;
        // Swap left and right motor speeds for correct turning in reverse
        double leftSpeed = distancePower + headingPower;
        double rightSpeed = distancePower - headingPower;

        rightMotors->move(rightSpeed);
        leftMotors->move(leftSpeed);
      }
      else
      {
        // Moving forward
        double leftSpeed = distancePower - headingPower;
        double rightSpeed = distancePower + headingPower;

        rightMotors->move(rightSpeed);
        leftMotors->move(leftSpeed);
      }

      // Break if within a small threshold of the target
      //  || fabs(headingError) < 2.0
      if (distanceError < lateralSettings.largeError)
      {
        move.give();
        break;
      }

      // Update previous errors
      prevDistanceError = distanceError;
      prevHeadingError = headingError;

      // Small delay
      xapi::delay(10);
    }

    // Stop the motors
    rightMotors->brake();
    leftMotors->brake();
    move.give();
  }

  void control::setMoveTo(double maxDis, double maxPower, double lowThresh)
  {
    this->nearlyMaxDis = maxDis;
    maxMoveSpeed = maxPower;
    this->lowThresh = lowThresh;
    // #define editedMove
    edited = true;
  }

  void control::slowCalc(double pcts, double dis, double drivePow)
  {
    // double goalPow = 0;
    double max = 0;
    if (dis > 14)
    {
      max = 11.9514;
    }
    else
    {
      max = 6;
    }
    if (increase != 0)
    {
      max = increase;
    }
    // printf("\nmax: ",max;
    double calcSpeed = 0.00191956 * pow(pcts, 4) - 0.0361944 * pow(pcts, 3) +
                       0.197441 * pow(pcts, 2) - 0.410973 * pcts + max;
    // goalPow = 0.00438555 * pow(pcts, 4) * -0.071259 * pow(pcts, 3) + 0.290144
    // * pow(pcts, 2) - 0.646639 * pcts
    // + 6.13426;

    // return goalPow; // slew(goalPow, drivePow, 0.25);
  };
  void control::simpleMoveTo(double x, double y, bool goingForward, bool initialTurn)
  {
    updateOdom();
    drivePIDv.reset();
    turnPIDv.reset();
    double errorX = x - ro.robotX;
    double errorY = y - ro.robotY;
    double distance = sqrt((double)pow((x - ro.robotX), 2) +
                           (double)pow((y - ro.robotY), 2));
    double targetHeading = atan2(errorX, errorY) * 180.0 / M_PI;
    if (!goingForward)
    {
      targetHeading = fmod(targetHeading + 180, 360);
    }
    printf("Target Heading: %f \n Going forward: %s", targetHeading, goingForward ? "true" : "false");
    if (abs(imu->get_heading() - targetHeading) > 6 && initialTurn)
    {
      turnTo(targetHeading);
    }
    if (!goingForward)
    {
      distance = -distance;
    }
    drivePID(distance);
  }
  void control::moveToPoint(Point point, bool goingForward)
  {
    updateOdom();
    drivePIDv.reset();
    turnPIDv.reset();
    leftMotors->set_zero_position(0);
    rightMotors->set_zero_position(0);
    double errorX = point.getX() - ro.robotX;
    double errorY = point.getY() - ro.robotY;
    double distance = sqrt((double)pow(errorX, 2) +
                           (double)pow(errorY, 2));
    double targetHeading = atan2(errorX, errorY) * 180.0 / M_PI;
    if (!goingForward)
    {
      targetHeading = fmod(targetHeading + 180, 360);
    }
    turnTo(targetHeading);
    int pidIter = 0;
    double error = 0, headingError = 0;
    double currentHeading = fmod(imu->get_rotation(), 360.0);
    double drivePower = 0, turnPower = 0;
    move.take(limit);
    while (true)
    {
      pidIter++;
      updateOdom();
      double currentL = leftMotors->get_position();
      double currentR = rightMotors->get_position();
      double average =
          calcInchesPerTick((currentL + currentR) / 2, wheelDiameter, gearRatio);
      errorX = point.getX() - ro.robotX;
      errorY = point.getY() - ro.robotY;
      targetHeading = atan2(errorX, errorY) * 180.0 / M_PI;
      double currentHeading = fmod(imu->get_rotation(), 360.0);
      distance = sqrt(errorX * errorX + errorY * errorY);
      error = distance - average;
      drivePower = drivePIDv.update(error);
      turnPower = turnPIDv.update(headingError);

      currentHeading = fmod(imu->get_rotation(), 360.0);

      if (targetHeading < 0)
      {
        targetHeading += 360;
      }
      if (!goingForward)
      {
        targetHeading = fmod(targetHeading + 180,
                             360); // Flip heading by 180 degrees to move backward
      }
      // Calculate heading error
      double headingError = targetHeading - currentHeading;
      if (pidIter % 20 == 0)
      {
        // printf("\n Travel: %f\n Timer Value %f\n Limit: %d", limit,
        //        positionAverage, Ti.getTimeLeft());
        printf("\n Error: %f", error);
        printf("\n DrivePower: %f", drivePower);
      }
      else if (pidIter > imax)
      {
        pidIter = 0;
      }

      if (error <= lateralSettings.largeError &&
          error >= -lateralSettings.largeError)
      {
        printf("\n Move to Point Ended with low error");
        break;
      }
      rightMotors->move(drivePower);
      leftMotors->move(drivePower);
      xapi::delay(10);
    }
    rightMotors->brake();
    leftMotors->brake();
    move.give();
  }
  void control::moveTo(double x, double y, double timeOut)
  {
    move.take(timeOut);
    moveCursor(1, 3);
    std::cout << "backward: " << backward
              << ", noInitialFIx: " << noInitialFIx
              << ", fixLateral: " << fixLateral
              << ", slowApproach: " << slowApproach
              << ", maxMoveSpeed: " << maxMoveSpeed
              << ", minMoveSpeed: " << minMoveSpeed
              << ", timeOut: " << timeOut;

    if (!edited)
    {
      nearlyMaxDis = 1.9;
      maxMoveSpeed = 100;
    }
    double goalX = x;
    double goalY = y;
    startMove = true;
    drivePIDv.reset();
    turnPIDv.reset();
    prevDis = sqrt((double)pow((goalX - ro.robotX), 2) +
                   (double)pow((goalY - ro.robotY), 2));
    double dis = prevDis;

    endGoalAngle = convertDegrees(atan2(goalX - ro.robotX, goalY - ro.robotY));
    printf("%f", endGoalAngle);
    if (timeOut == 0)
    {
      limit = std::abs((endGoalAngle / 100) + (prevDis / 10)) / 2;
    }
    else
    {
      limit = timeOut;
    }

    double yDif = goalY - ro.robotY;
    double xDif = goalX - ro.robotX;

    printf("\n GoalX: %f \t GoalY: %f \t limit: %f \t yDif: %f \t xDif: %f",
           goalX, goalY, limit, yDif, xDif);
    // double startingAngle = ro.robotAngle;
    if (yDif < 2)
    {
      endGoalAngle = -convertDegrees(atan2(goalX - ro.robotX, 0));
    }
    if (backward)
    {
      checkSign(endGoalAngle) == 1 ? endGoalAngle = endGoalAngle - 180
                                   : endGoalAngle = endGoalAngle + 180;
    }
    if (!noInitialFIx)
    {
      turnTo(endGoalAngle);
    }
    if (dis < lowThresh)
    {
      printf("\nMoveTo cannot sucessfully move less than %f inches\n Defaulting "
             "to drivePID\n",
             lowThresh);
      drivePID(dis);
    }

    double maxDis = 0;
    // bool belowFive = false;
    int moveIter = 0;
    bool minned = false;
    double pctTraveled = 0.0;
    double traveled = 0.0;
    double currentTime = xapi::millis() + limit * 1000;
    double drivePower, turnPower, turnError;
    double goalPosAngle, difPosGoalAngle;
    double distance;
    double MoreExtras, MainError;
    while (startMove && xapi::millis() < currentTime)
    {
      updateOdom();
      moveIter++;
      double distance = sqrt((double)pow((goalX - ro.robotX), 2) +
                             (double)pow((goalY - ro.robotY), 2));
      traveled += distance - prevDis;
      pctTraveled = fabs((traveled / dis) * 10);
      if (distance > maxDis)
      {
        maxDis = distance;
      }
      double currentAngle = ro.robotAngle;
      turnError = (endGoalAngle - currentAngle);
      drivePower = drivePIDv.update(distance);
      turnPower = turnPIDv.update(turnError);

      // slowCalc(pctTraveled, dis, distance);
      drivePower = slew(drivePower, prevDrivePower, lateralSettings.slew);

      if (drivePower <= minMoveSpeed)
      {
        drivePower = minMoveSpeed;
      }
      if (drivePower >= maxMoveSpeed)
      {
        drivePower = maxMoveSpeed;
      }
      else if (drivePower <= minMoveSpeed && !minned && !useNewSlowApproach)
      {
        drivePower = minMoveSpeed;
        minned = true;
      }
      if (distance < 12 && drivePower - minMoveSpeed < 1 && dis > 16 &&
          !useNewSlowApproach)
      {
        minned = true;
        drivePower = minMoveSpeed;
      }

      // slowApproach = true ? drivePower = slew(drivePower, prevDrivePower,
      // tmpMax / (1 / distance)) : drivePower += 0;
      if (distance < nearlyMaxDis)
      {
        // slowApproach = true ? drivePower = slew(drivePower, prevDrivePower,
        // tmpMax / distance) : drivePower += 0;
        drivePower = drivePIDv.update(distance) * cos(convertRadians(turnError));
        turnPower =
            convertDegrees(atan(tan(convertRadians(turnError)))) * turnPIDv.P;
      }

      if (moveIter % 10 == 0 || moveIter < 2)
      {
        printf("Angle Data: \n Goal Angle: %f \n turnPower: %f \n turnError: %f",
               endGoalAngle, turnPower, turnError);
        printf("%f\n\n Linear Data: \n drivePower: %f \t distance: %f \t ",
               distance, drivePower, distance);
        printf("goalX: %f \t goalY: %f\n\n Pct traveled: %f", goalX, goalY,
               pctTraveled);
        // printf("\ncalcSpeed: %f", calcSpeed);

        printf("\n\n RobotX: %f \t RobotY: %f \t RobotAngle %f\n", ro.robotX,
               ro.robotY, ro.robotAngle);
      }
      else if (moveIter >= imax)
      {
        moveIter = 0;
      }

      if (distance < 2 || pctTraveled > 9.5)
      {
        printf("Angle Data: \n Goal Angle: %f \n turnPower: %f \n turnError: "
               "%f\n\n Linear Data: %f \n drivePower: %f \t distance: %f \t "
               "goalX: %f \t goalY: %f\n\n Pct traveled: %f",
               endGoalAngle, turnPower, turnError, distance, drivePower, distance,
               goalX, goalY, pctTraveled);
        printf("\n\n RobotX: %f \t RobotY: %f \t RobotAngle %f\n", ro.robotX,
               ro.robotY, ro.robotAngle);

        // printf("\ncalcSpeed: %f", calcSpeed);
        printf("\n MoveTo has finished, Stopping Robot...");
        // printf("\n MoveTo took %d seconds to complete.", Ti.getTimeLeft());
        rightMotors->brake();
        leftMotors->brake();

        // backLeftMotor->brake();
        startMove = false;
        // turnTo(endGoalAngle);
      }
      else if (backward)
      {
        leftMotors->move(-(drivePower + turnPower));
        rightMotors->move(-(drivePower - turnPower));
        maxDis = dis;
      }
      else
      {
        leftMotors->move((drivePower + turnPower));
        rightMotors->move(drivePower - turnPower);
      }
      prevDrivePower = drivePower;

      prevDis = distance;
      xapi::delay(20);
    }
    move.give();
    rightMotors->brake();
    leftMotors->brake();
  }

  void control::moveTo(double x, double y, double limits, moveToParams params)
  {
    slowApproach = !params.noSlowApproach;
    if (params.maxSpeed == 0)
    {
      params.maxSpeed = 12;
    }
    if (params.minSpeed == 0)
    {
      params.minSpeed = 3;
    }
    maxMoveSpeed = params.maxSpeed;
    minMoveSpeed = params.minSpeed;

    useNewSlowApproach = params.useNewSlowApproach;
    backward = params.backwards;
    noInitialFIx = params.noInitialFIx;
    moveTo(x, y, limits);
    if (params.doEndAngle)
    {
      turnTo(params.endAngle);
    }
    params.noSlowApproach = 1;
    params.maxSpeed = 12;
    params.minSpeed = 3;
    params.backwards = 0;
    params.noInitialFIx = 0;
    params.doEndAngle = 0;
    params.endAngle = 0;
    params.useNewSlowApproach = 0;
  }

  Point control::getPose(bool radians, bool standardPos)
  {
    Point pose = getPose(true);
    if (standardPos)
      pose.theta = M_PI_2 - pose.theta;
    if (!radians)
      pose.theta = radToDeg(pose.theta);
    return pose;
  }

  void control::moveToPath()
  {
    bool path = true;
    int CurrentPoint = 0;
    // std::vector<Point> movePointArray;
    double DistanceToPoint;
    Point ClosestPoint = Point(ro.robotX, ro.robotY);
    movePointArray.push_back(movePointArray.at(movePointArray.size() - 1));
    while (path)
    {
      ClosestPoint = movePointArray.at(CurrentPoint);
      DistanceToPoint =
          sqrt(pow((movePointArray.at(CurrentPoint).getX() - ro.robotX), 2) +
               pow((movePointArray.at(CurrentPoint).getY() - ro.robotY), 2));

      // printf("%s,
      //          ,"Distance to Closest Point: ",DistanceToPoint,"\t"
      //          ,"Index: ",CurrentPoint,"\n")
      //          ,"Current Point: ";
      ClosestPoint.ExportPoint();
      if (CurrentPoint < movePointArray.size() - 1)
      {
        moveTo(ClosestPoint.getX(), ClosestPoint.getY());
        CurrentPoint++;
      }
      else
      {
        // printf("\n Finished Path. Ending loop... ";
        path = false;
      }
      xapi::delay(1);
      updateOdom();
    }
  }

  void control::dynamicTuning(double type)
  {
    bool tuneDrive = type = 0 ? 0 : 1;
    double tmpP = 1, tmpI = 0, tmpD = 0;
    if (tuneDrive)
    {
      Point start = {ro.robotX, ro.robotY};
      double startingAngle = ro.robotAngle;
      drivePID(10);
      Point endPos = {cos(convertRadians(startingAngle)) * 10,
                      sin(convertRadians(startingAngle)) * 10};
      double errorX = (endPos.x - start.x);
      double errorY = (endPos.x - start.x);
      bool worseXError = errorX > errorY ? 1 : 0;
      if (worseXError && errorX > 3)
      {
        if (errorX > 5 && !(tmpP < 1))
        {
          tmpP += (errorX / 10);
        }
        else
        {
          tmpD += (errorX / 10);
        }
        if (errorX >= 10)
        {
          tmpI += (errorX / 100);
        }
      }
      else if (errorY > 3)
      {
        if (errorY > 5 && !(tmpP < 1))
        {
          tmpP += (errorY / 10);
        }
      }
      // printf("Current P: ",tmpP
      //          ,"Current I: ",tmpI
      //          ,"Current D: ",tmpD;
    }
    else if (type == 2)
    {
      /// Tune Helper
    }
    else if (type == 3)
    {
      // Tune moveTo
    }
    else
    {
      double startingAngle = ro.robotAngle;
      turnPID(90);
      double turnError = (startingAngle + 90) - ro.robotAngle;

      if (turnError > 10)
      {
        tmpP += turnError / 10;
      }
      else if (turnError > 5)
      {
        tmpP += turnError / 100;
      }

      if (turnError < 10)
      {
        tmpP -= turnError / 10;
      }
      else if (turnError < 5)
      {
        tmpP += turnError / 100;
      }

      // printf("Current P: ",tmpP
      //          ,"Current I: ",tmpI
      //          ,"Current D: ",tmpD;
    }
  }

  void control::dynamicMoveTo() {}

  void control::setSwingPID(pidValue pidVal) { swingPID = pidVal; }

  // Swing Code Created by Zachary Layland
  //
  void control::right_swing(float angle, float oppositeSpeed)
  {
    turnError = (angle)-ro.robotAngle;
    while (swingPID.is_settled() == false)
    {
      float error = (angle - ro.robotAngle);
      float output = swingPID.update(error);
      // output = clamp(output, -turn_max_voltage, turn_max_voltage);
      leftMotors->move(fabs(output));
      rightMotors->move(fabs(output * oppositeSpeed));
      xapi::delay(10);
    }
    rightMotors->brake();
    leftMotors->brake();
  }

  void control::left_swing(float angle, float oppositeSpeed)
  {
    // if (dir == vex::directionType::fwd && angle < 0)
    // {
    //     angle += 360;
    // }
    bool use = true;
    while (use)
    {
      turnError = angle - ro.robotAngle;
      float output = turnPIDv.update(turnError);
      // output = clamp(output, -turn_max_voltage, turn_max_voltage);
      printf("Error: %f\tOutput: %f\n", turnError, output);
      leftMotors->move(fabs(output));
      rightMotors->move(-fabs(output * oppositeSpeed));
      // if (Error < 1) {
      //   use = false;
      // }
      xapi::delay(10);
    }
    rightMotors->brake();
    leftMotors->brake();
  }

  void control::moveToBoom(double x, double y, double theta, double lead,
                           double timeOut)
  {
    move.take(timeOut);
    bool atTarget = false;
    int iterBoom = 0;
    double traveled = 0.0;
    double pctTraveled = 0.0;
    double dis =
        sqrt((double)pow((x - ro.robotX), 2) + (double)pow((y - ro.robotY), 2));
    double the = theta;
    double prevDis = 0;
    double prevDrive = 0;
    bool firstLarge = false;
    double leftPower = 0;
    double rightPower = 0;
    double turnPower = 0;
    float angleError = 0;
    bool latDone = false;
    bool latNearDone = false;
    bool angleClose = false;
    float lateralError = 0;
    drivePIDv.reset();
    turnPIDv.reset();
    increase = 50;
    float targetTheta =
        fmod(convertDegrees(atan2(x - ro.robotX, y - ro.robotY)), 360);
    bool atEnd = false;
    double currentTime =
        xapi::millis() +
        (lateralSettings.largeErrorTimeout + angularSettings.largeErrorTimeout) +
        750 * (dis / 24);
    if (timeOut != 0)
    {
      currentTime = timeOut;
    }
    double lowestDis = dis;
    while (!atTarget)
    {
      iterBoom++;
      double distance =
          sqrt((double)pow((x - ro.robotX), 2) + (double)pow((y - ro.robotY), 2));

      double carrotX = x - lead * (distance * cos(the));
      double carrotY = y - lead * (distance * sin(the));

      float deltaX = carrotX - ro.robotX;
      float deltaY = carrotY - ro.robotY;

      targetTheta = fmod(
          convertDegrees(atan2(carrotX - ro.robotX, carrotY - ro.robotY)), 360);
      bool turnLeft = (fmod(ro.robotAngle, 360) - 180) - targetTheta >=
                      (fmod(ro.robotAngle, 360) + 180) - targetTheta;

      float hypot = std::hypot(deltaX, deltaY);
      float diffTheta1 = targetTheta - fmod(ro.robotAngle, 360);
      float diffTheta2 = theta - fmod(ro.robotAngle, 360);
      double turnPo = fabs(cos(angleError));
      if (hypot < lowestDis)
      {
        lowestDis = hypot;
      }
      if (turnPo < 0.05 && hypot > lateralSettings.largeError)
      {
        lateralError = hypot;
      }
      else
      {
        lateralError = hypot * turnPo;
      }
      double drivePower = drivePIDv.update(lateralError);
      if ((hypot <= lateralSettings.largeError &&
           hypot >= -lateralSettings.largeError) ||
          latNearDone)
      {
        latNearDone = true;
      }
      if ((hypot <= lateralSettings.largeError &&
           hypot >= -lateralSettings.largeError) ||
          latDone)
      {
        latDone = true;
      }

      if (latNearDone || angleClose)
      {
        angleError = diffTheta2;
        drivePower = slew(drivePower, prevDrive, lateralSettings.slew);
      }
      else
      {
        angleError = diffTheta1;
      }
      drivePower = slew(drivePower, prevDrive, lateralSettings.slew);
      if (latDone)
      {
        drivePower = 0;
      }

      turnPower = turnPIDv.update(angleError);

      if (drivePower <= minMoveSpeed && (!latNearDone))
      {
        drivePower = minMoveSpeed;
      }
      if (drivePower >= maxMoveSpeed)
      {
        drivePower = maxMoveSpeed;
      }

      if (iterBoom % 30 == 0 || iterBoom <= 3)
      {
        printf("\ncarrot: (%f, %f)", carrotX, carrotY);
        printf("\ndistance: %f", distance);
        printf("\n RobotX: %f RobotY: %f RobotAngle: %f", ro.robotX, ro.robotY,
               ro.robotAngle);
        printf("\n targetTheta: %f", targetTheta);
        printf("\n theta: %f", the);
        printf("\n hypot: %f", hypot);
        printf("\n turnPo: %f", turnPo);

        printf("\n angleError: %f", angleError);
        printf("\n lateralError: %f\n", lateralError);

        printf("\n drivePower: %f", drivePower);
        printf("\n turnPower: %f\n", turnPower);
        printf("goalX: %f \t goalY: %f", x, y);
      }
      else if (iterBoom > imax)
      {
        iterBoom = 0;
      }
      // Move the motors
      if (!turnLeft)
      {

        leftPower = drivePower - turnPower;
        rightPower = drivePower + turnPower;
      }
      else
      {
        leftPower = drivePower + turnPower;
        rightPower = drivePower - turnPower;
      }
      // Log data
      if ((lateralError <= lateralSettings.largeError &&
           lateralError >= -lateralSettings.largeError &&
           angleError <= angularSettings.largeError * 2 &&
           angleError >= -angularSettings.largeError * 2))
      {
        if (firstLarge == false)
        {
          firstLarge = true;
          printf("Boomerang in large error range, slowing down ...\n");
          latNearDone = true;
          angleClose = true;
        }
        if ((lateralError <= lateralSettings.smallError &&
             lateralError >= -lateralSettings.smallError &&
             angleError <= angularSettings.smallError &&
             angleError >= -angularSettings.smallError) ||
            (xapi::millis() >= currentTime))
        {
          printf("Boomerang in small error range ...\n");
          printf("\ncarrot: (%f, %f)", carrotX, carrotY);
          printf("\ndistance: %f", distance);
          printf("\n RobotX: %f RobotY: %f RobotAngle: %f", ro.robotX, ro.robotY,
                 ro.robotAngle);
          printf("\n targetTheta: %f", targetTheta);
          printf("\n theta: %f", the);
          printf("\n hypot: %f", hypot);
          printf("\n turnPo: %f", turnPo);

          printf("\n angleError: %f", angleError);
          printf("\n lateralError: %f\n", lateralError);

          printf("\n drivePower: %f", drivePower);
          printf("\n turnPower: %f\n", turnPower);
          printf("goalX: %f \t goalY: %f", x, y);
          atTarget = true;
          leftMotors->brake();
          rightMotors->brake();
          while (leftMotors->get_actual_velocity() != 0 ||
                 rightMotors->get_actual_velocity() != 0)
          {
            xapi::delay(10);
          }
          atTarget = true;
        }
        else
        {
          if (iterBoom % 20 == 0)
          {
            printf("\ncarrot: (%f, %f)", carrotX, carrotY);
            printf("\ndistance: %f", distance);
            printf("\n RobotX: %f RobotY: %f RobotAngle: %f", ro.robotX,
                   ro.robotY, ro.robotAngle);
            printf("\n targetTheta: %f", targetTheta);
            printf("\n theta: %f", the);
            printf("\n hypot: %f", hypot);
            printf("\n turnPo: %f", turnPo);

            printf("\n angleError: %f", angleError);
            printf("\n lateralError: %f\n", lateralError);

            printf("\n drivePower: %f", drivePower);
            printf("\n turnPower: %f\n", turnPower);
            printf("goalX: %f \t goalY: %f", x, y);
          }
          leftMotors->move(leftPower);
          rightMotors->move(rightPower);
        }
      }
      else
      {
        leftMotors->move(leftPower);
        rightMotors->move(rightPower);
      }

      prevDis = distance;
      prevDrive = drivePower;
      xapi::delay(10);
    }
    move.give();
  }
  void control::moveToBoom(double *xr, double *yr, double *thetar, double *leadr,
                           double *timeOutr)
  {
    //  xapi::Task task{[=]{}};

    move.take(*timeOutr);
    double x = *xr;
    double y = *yr;
    double theta = *thetar;
    double lead = *leadr;
    double timeOut = *timeOutr;
    bool atTarget = false;
    int iterBoom = 0;
    double traveled = 0.0;
    double pctTraveled = 0.0;
    double dis =
        sqrt((double)pow((-ro.robotX), 2) + (double)pow((y - ro.robotY), 2));
    double the = theta;
    double prevDis = 0;
    double prevDrive = 0;
    bool firstLarge = false;
    double leftPower = 0;
    double rightPower = 0;
    double turnPower = 0;
    float angleError = 0;
    bool latDone = false;
    bool latNearDone = false;
    bool angleClose = false;
    float lateralError = 0;
    drivePIDv.reset();
    turnPIDv.reset();
    increase = 50;
    float targetTheta =
        fmod(convertDegrees(atan2(x - ro.robotX, y - ro.robotY)), 360);
    bool atEnd = false;
    double currentTime =
        xapi::millis() +
        (lateralSettings.largeErrorTimeout + angularSettings.largeErrorTimeout) +
        750 * (dis / 24);
    if (timeOut != 0)
    {
      currentTime = timeOut;
    }
    double lowestDis = dis;
    while (!atTarget)
    {
      iterBoom++;
      double distance =
          sqrt((double)pow((x - ro.robotX), 2) + (double)pow((y - ro.robotY), 2));

      double carrotX = x - lead * (distance * cos(the));
      double carrotY = y - lead * (distance * sin(the));

      float deltaX = carrotX - ro.robotX;
      float deltaY = carrotY - ro.robotY;

      targetTheta = fmod(
          convertDegrees(atan2(carrotX - ro.robotX, carrotY - ro.robotY)), 360);
      bool turnLeft = (fmod(ro.robotAngle, 360) + 180) - targetTheta >=
                      (fmod(ro.robotAngle, 360) - 180) - targetTheta;

      float hypot = std::hypot(deltaX, deltaY);
      float diffTheta1 = targetTheta - fmod(ro.robotAngle, 360);
      float diffTheta2 = theta - fmod(ro.robotAngle, 360);
      double turnPo = fabs(cos(angleError));
      if (hypot < lowestDis)
      {
        lowestDis = hypot;
      }
      if (turnPo < 0.05 && hypot > lateralSettings.largeError)
      {
        lateralError = hypot;
      }
      else
      {
        lateralError = hypot * turnPo;
      }
      double drivePower = drivePIDv.update(lateralError);
      if ((hypot <= lateralSettings.largeError &&
           hypot >= -lateralSettings.largeError) ||
          latNearDone)
      {
        latNearDone = true;
      }
      if ((hypot <= lateralSettings.largeError &&
           hypot >= -lateralSettings.largeError) ||
          latDone)
      {
        latDone = true;
      }

      if (latNearDone || angleClose)
      {
        angleError = diffTheta2;
        drivePower = slew(drivePower, prevDrive, lateralSettings.slew);
      }
      else
      {
        angleError = diffTheta1;
      }
      drivePower = slew(drivePower, prevDrive, lateralSettings.slew);
      if (latDone)
      {
        drivePower = 0;
      }

      turnPower = turnPIDv.update(angleError);

      if (drivePower <= minMoveSpeed && (!latNearDone))
      {
        drivePower = minMoveSpeed;
      }
      if (drivePower >= maxMoveSpeed)
      {
        drivePower = maxMoveSpeed;
      }

      if (iterBoom % 30 == 0 || iterBoom <= 3)
      {
        printf("\ncarrot: (%f, %f)", carrotX, carrotY);
        printf("\ndistance: %f", distance);
        printf("\n RobotX: %f RobotY: %f RobotAngle: %f", ro.robotX, ro.robotY,
               ro.robotAngle);
        printf("\n targetTheta: %f", targetTheta);
        printf("\n theta: %f", the);
        printf("\n hypot: %f", hypot);
        printf("\n turnPo: %f", turnPo);

        printf("\n angleError: %f", angleError);
        printf("\n lateralError: %f\n", lateralError);

        printf("\n drivePower: %f", drivePower);
        printf("\n turnPower: %f\n", turnPower);
        printf("goalX: %f \t goalY: %f", x, y);
      }
      else if (iterBoom > imax)
      {
        iterBoom = 0;
      }
      // Move the motors
      if (!turnLeft)
      {

        leftPower = drivePower - turnPower;
        rightPower = drivePower + turnPower;
      }
      else
      {
        leftPower = drivePower + turnPower;
        rightPower = drivePower - turnPower;
      }
      // Log data
      if ((lateralError <= lateralSettings.largeError &&
           lateralError >= -lateralSettings.largeError &&
           angleError <= angularSettings.largeError * 2 &&
           angleError >= -angularSettings.largeError * 2))
      {
        if (firstLarge == false)
        {
          firstLarge = true;
          printf("Boomerang in large error range, slowing down ...\n");
          latNearDone = true;
          angleClose = true;
        }
        if ((lateralError <= lateralSettings.smallError &&
             lateralError >= -lateralSettings.smallError &&
             angleError <= angularSettings.smallError &&
             angleError >= -angularSettings.smallError) ||
            (xapi::millis() >= currentTime))
        {
          printf("Boomerang in small error range ...\n");
          printf("\ncarrot: (%f, %f)", carrotX, carrotY);
          printf("\ndistance: %f", distance);
          printf("\n RobotX: %f RobotY: %f RobotAngle: %f", ro.robotX, ro.robotY,
                 ro.robotAngle);
          printf("\n targetTheta: %f", targetTheta);
          printf("\n theta: %f", the);
          printf("\n hypot: %f", hypot);
          printf("\n turnPo: %f", turnPo);

          printf("\n angleError: %f", angleError);
          printf("\n lateralError: %f\n", lateralError);

          printf("\n drivePower: %f", drivePower);
          printf("\n turnPower: %f\n", turnPower);
          printf("goalX: %f \t goalY: %f", x, y);
          atTarget = true;
          leftMotors->brake();
          rightMotors->brake();
          while (leftMotors->get_actual_velocity() != 0 ||
                 rightMotors->get_actual_velocity() != 0)
          {
            xapi::delay(10);
          }
          atTarget = true;
        }
        else
        {
          if (iterBoom % 20 == 0)
          {
            printf("\ncarrot: (%f, %f)", carrotX, carrotY);
            printf("\ndistance: %f", distance);
            printf("\n RobotX: %f RobotY: %f RobotAngle: %f", ro.robotX,
                   ro.robotY, ro.robotAngle);
            printf("\n targetTheta: %f", targetTheta);
            printf("\n theta: %f", the);
            printf("\n hypot: %f", hypot);
            printf("\n turnPo: %f", turnPo);

            printf("\n angleError: %f", angleError);
            printf("\n lateralError: %f\n", lateralError);

            printf("\n drivePower: %f", drivePower);
            printf("\n turnPower: %f\n", turnPower);
            printf("goalX: %f \t goalY: %f", x, y);
          }
          leftMotors->move(leftPower);
          rightMotors->move(rightPower);
        }
      }
      else
      {
        leftMotors->move(leftPower);
        rightMotors->move(rightPower);
      }

      prevDis = distance;
      prevDrive = drivePower;
      xapi::delay(10);
    }
    move.give();
  }
  void control::printCoords()
  {
    printf("\n x: %f y: %f theta: %f", ro.robotX, ro.robotY, ro.robotAngle);
  }
  void control::setPos(double x, double y, double theta)
  {
    ro.set(x, y);
    imu->set_rotation(theta);
  }
} // namespace xVecLib
