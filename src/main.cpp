#define PRO
#define PROS_USE_SIMPLE_NAMES

#include "xVecLib/provex.hpp"
#include "main.h"
#include "xVecLib/structure.hpp"
#include "xVecLib/xVecLib.hpp"

using namespace xVecLib;

pros::Controller master(pros::E_CONTROLLER_MASTER);
xapi::MotorGroup right_mg({-1, -2, -5}); // Creates a motor group with 18
xapi::MotorGroup left_mg({8, 9, 10});    // Creates a motor group with 11
xapi::IMU imu = xapi::IMU(6);
pros::MotorGroup intake({11, 20});
pros::MotorGroup rise({4});
pros::Optical optical(10);
pidSettings
    linearController(5,   // P
                     0,   // I
                     10,  // D
                     3,   // Max I
                     0.8, // small error range, in inches
                     2.5, // large error range, in inches
                     5    // maximum acceleration (slew)
    );
// 8 0 20 3 1 100 3 500 5

// angular motion controller
double exactTP = (double)37 / 18;
pidSettings
    angularController(exactTP, // proportional gain (kP)
                      0,       // integral gain (kI)
                      6,       // derivative gain (kD)
                      3,       // anti windup
                      1,       // small error range, in degrees //1.7
                      6,       // large error range, in degrees
                      5        //(slew)
    );

robot hs = robot(&right_mg, &left_mg, &imu, linearController, angularController,
                 3.5, 2);
pros::adi::DigitalOut largeClamp = pros::adi::DigitalOut('C');
pros::adi::DigitalOut goal = pros::adi::DigitalOut('G');
pros::adi::DigitalOut doinker = pros::adi::DigitalOut('F');

// pros::adi::DigitalOut park = pros::adi::DigitalOut('C');

/**j
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize()
{
  hs.setMoveTo(8, 127, 12);
  hs.setOdomAngle(y);
  left_mg.set_zero_position_all(0);
  right_mg.set_zero_position_all(0);
  hs.setSwingPID({3, 0, 8});
  while (imu.is_calibrating())
  {
    pros::delay(10);
  }
  pros::screen::set_eraser(0x000000);
  pros::screen::erase();
  pros::Task screenTask([&]()
                        {
        while (true) {  
            pros::screen::set_pen(0xFFFFFF); // print robot location to the brain screen
            pros::screen::print(pros::E_TEXT_MEDIUM, 2, "X: %f", ro.robotX);
            pros::screen::print(pros::E_TEXT_MEDIUM, 3, "Y: %f", ro.robotY);
            pros::screen::print(pros::E_TEXT_MEDIUM, 4, "Theta: %f", ro.Theta);
            pros::screen::print(pros::E_TEXT_MEDIUM, 5, "Angle: %f", ro.robotAngle);
            hs.updateOdom();
            
          pros::delay(50);    
        } });
  while (imu.is_calibrating())
  {
    pros::delay(10);
  }
  left_mg.set_brake_mode_all(pros::E_MOTOR_BRAKE_HOLD);
  right_mg.set_brake_mode_all(pros::E_MOTOR_BRAKE_HOLD);
  rise.tare_position();
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {}

void intakeFix(double mili)
{
  intake.move(127);
  pros::delay(mili);
  intake.move(-125);
  pros::delay(mili / 10);
}
void intakeFix(int count)
{
  for (int i = 0; i < count; i++)
  {
    intake.move(127);
    pros::delay(1000);
    intake.move(-125);
    pros::delay(100);
    intake.move(127);
  }
}
/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {}

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous()
{
  int auton = 6;
  // Delay to allow the robot to finalize posistion before starting
  xapi::delay(100);

  /* Test Library */
  if (auton == 0)
  {
    hs.drivePID(24);
    hs.drivePID(-24);
    hs.setPos(0, 0, 0);
    hs.turnTo(-90);

    hs.turnTo(90);
    hs.turnTo(0);
    // hs.setPos(-48, -57,0);
    hs.moveToBoom(24, 25, 27, 0.5);
    hs.setPos(0, 0, 0.1);
    hs.printCoords();
    hs.moveToBoom(22.609, 23.034, 133.6, 0.37685601176242045, 5000);
    hs.moveToBoom(24.068, 0.525, -116.82300000000001, 0.1917876026855742, 5000);
    hs.moveToBoom(0.308, -23.86, -17.854025751516758, 0.374442462386121, 5000);
    //   hs.moveToBoom(24,24,0,.2,4000);
    //   hs.turnTo(180);
    //   hs.drivePID(24);
    // hs.curvePID(24, 24, 1, 1);
    // hs.simpleMoveTo(24,24);
  }

  /*Solo AWP blue*/
  if (auton == 1)
  {
    hs.setPos(-60, -12, -68);
    hs.printCoords();
    hs.drivePID(-40);
    goal.set_value(true);
    intake.move(125);
    pros::delay(1000);
    hs.drivePID(28);
    hs.turnTo(0);
    // hs.drivePID(30);
    // hs.turnTo(-125);
    // hs.drivePID(20);
    // pros::delay(1000);
    // hs.turnTo(125);
    // pros::delay(1000);
    // intake.brake();

    // hs.drivePID(63);
    // hs.turnTo(270);
    // hs.drivePID(38);
    // intake.brake();
    // hs.turnTo(36);
    // hs.drivePID(-24);
    // goal.set_value(false);
    // hs.drivePID(12);
    // hs.turnTo(90);
    // intake.move(125);
    // hs.drivePID(36);
    // intake.brake();
    // hs.turnTo(270);
    // hs.drivePID(-10);
    // goal.set_value(true);
    // intake.move(125);
    // pros::delay(1000);
    // intake.brake();
    // hs.turnTo(0);
    // hs.drivePID(20);
  }
  /* Skills */
  if (auton == 2)
  {
    hs.setPos(-58, 24, 233);
    hs.printCoords();
    //     intake.move(125);
    //     pros::delay(400);

    //     hs.drivePID(10);
    // hs.turnTo(0); //?
    // hs.drivePID(-22);
    hs.drivePID(-8);
    goal.set_value(true);
    intake.move(127);
    pros::delay(500);

    // Score 6 rings on goal and putin corner
    hs.turnTo(98);

    hs.drivePID(36);
    pros::delay(500);
    hs.turnTo(0);
    pros::delay(200);
    hs.drivePID(26);
    pros::delay(500);

    intake.move(-50);
    pros::delay(800);
    intake.move(127);
    pros::delay(1000);

    hs.turnTo(-90);

    hs.drivePID(24);
    pros::delay(1000);
    intake.move(-50);
    pros::delay(800);
    intake.move(127);
    pros::delay(1000);
    hs.drivePID(16);
    pros::delay(1000);
    intake.move(-50);
    pros::delay(800);
    intake.move(127);
    pros::delay(800);

    hs.turnTo(27);
    pros::delay(500);
    hs.drivePID(20, 1000.0);
    pros::delay(500);

    intake.move(-50);
    pros::delay(400);
    intake.move(127);
    pros::delay(1000);

    pros::delay(500);
    hs.turnTo(120);
    intake.move(-110);

    // score 1st goal
    pros::delay(200);
    left_mg.move(-100);
    right_mg.move(-100);
    pros::delay(1000);
    goal.set_value(false);
    pros::delay(200);
    hs.drivePID(20); // More

    // travel to 2nd goal
    pros::delay(200);

    // hs.turnTo(170);
    // Need to determine if position is correct for the 2nd goal
    double tmp = fmod(convertDegrees(atan2((-24) - ro.robotX, (47) - ro.robotY)), 360);
    if (tmp - 180 < 15 && tmp - 180 > -15)
    {
      hs.turnTo(tmp);
    }
    else
    {
      hs.turnTo(180);
    }
    hs.turnTo(180);
    hs.drivePID(60);
    pros::delay(500);
    hs.turnTo(190);
    hs.drivePID(60);
    //  goal.set_value(true);

    intake.brake();

    // score 2nd goal
    hs.turnTo(200);
    hs.drivePID(53, 2000.0);
    pros::delay(300);
    hs.turnPID(90);
    hs.drivePID(10);
    pros::delay(300);
    hs.drivePID(-9);

    // Travel to third goal
    hs.turnTo(-90); // might need to be negative for clamp
    pros::delay(300);
    hs.turnTo(-100); // might need to be negative for clamp
    hs.drivePID(-60);
    pros::delay(300);
    hs.turnTo(-110);
    hs.drivePID(-60);
    pros::delay(300);
    hs.drivePID(-15); // Might need to be bigger
    hs.turnTo(200);
    pros::delay(300);
    hs.turnTo(165);
    hs.drivePID(-30);
    goal.set_value(true);
    pros::delay(300);

    // Score third Goal
    hs.drivePID(35, 1500.0);
    hs.turnTo(-110);
    hs.drivePID(-20, 1000.0);
    goal.set_value(false);
    pros::delay(300);

    // travel to 4th goal
    hs.drivePID(20);
    hs.turnTo(0);
    hs.drivePID(60);
    pros::delay(300);
    hs.turnTo(15);
    hs.drivePID(60);

    pros::delay(300);

    hs.turnTo(180);
    hs.drivePID(-30);
    goal.set_value(true);
    pros::delay(300);
    hs.drivePID(-40, 1300.0);
    hs.turnTo(-90);
    hs.drivePID(-20, 1300.0);
    goal.set_value(false);
    //     hs.turnTo(50); //fix
    //     hs.drivePID(10);

    //     // Put 3rd goal in corner
    //     hs.turnTo(153);
    //     hs.drivePID(-15);
    //     goal.set_value(true);
    //     pros::delay(300);
    // hs.turnTo(42);
    //     hs.drivePID(-40);

    // hs.simpleMoveTo(0, -58, 1);
    // pros::delay(300);
    // hs.turnTo(0);
    // hs.drivePID(12);

    // // Get other 4
    // hs.turnTo(270);
    // hs.drivePID(60);
    // pros::delay(300);
    // hs.turnTo(127);
    // hs.drivePID(24);
    // hs.turnTo(81);
    // hs.drivePID(-24);
    // goal.set_value(false);
    // hs.drivePID(24);

    // // Travel to goal with blue ring 1
    // // hs.curvePID(-45, -26, 0, 0);
    // hs.simpleMoveTo(-45, -26, 1);
    // goal.set_value(true);
    // hs.turnTo(342);
    // hs.drivePID(-40);
    // goal.set_value(false);

    // hs.simpleMoveTo(58, 8, 1);
    // goal.set_value(true);
    // hs.turnTo(180);
    // hs.drivePID(-44);
    // hs.turnTo(227);
    // hs.drivePID(-24);
    // goal.set_value(false);
    // hs.drivePID(24);
    // hs.simpleMoveTo(30, 11, 1);
  }
  if (auton == 3)
  {
    hs.setPos(-58, 24, 225);
    hs.printCoords();
    rise.move(-80);
    pros::delay(500);
    rise.move_absolute(0, 200);
    hs.drivePID(-8);
    goal.set_value(true);
    hs.turnTo(88);
    intake.move(-120);
    hs.drivePID(38);
    pros::delay(500);
    hs.turnTo(0);
    pros::delay(200);
    hs.drivePID(28);
    pros::delay(500);

    intake.move(-50);
    pros::delay(800);
    intake.move(127);
    pros::delay(1000);

    hs.turnTo(90);

    hs.drivePID(50);

    pros::delay(500);
    hs.turnTo(285);
    pros::delay(1000);
    intake.move(-50);
    pros::delay(800);
    intake.move(127);
    pros::delay(1000);
    hs.drivePID(16);
    pros::delay(1000);
    intake.move(-50);
    pros::delay(800);
    intake.move(127);
    pros::delay(800);

    hs.turnTo(27);
    pros::delay(500);
    hs.drivePID(20, 1000.0);
    pros::delay(500);

    intake.move(-50);
    pros::delay(400);
    intake.move(127);
    pros::delay(1000);

    pros::delay(500);
    hs.turnTo(120);
    intake.move(-110);

    // score 1st goal
    pros::delay(200);
    left_mg.move(-100);
    right_mg.move(-100);
    pros::delay(1000);
    goal.set_value(false);
    pros::delay(200);
    hs.drivePID(20); // More

    /* code */
  }
  // Near SIde Left Auto
  if (auton == 5)
  {
    hs.setPos(-51, -59, 0);
    hs.printCoords();
    hs.moveToBoom(-6.084, -59.269, 0, 0, 5000);
    hs.moveToBoom(-16.896, -48.217, -90, 0, 5000);
    hs.moveToBoom(-24.584, -48.217, 0, 0, 5000);
    hs.moveToBoom(-23.623, -30.677, -177.8000000000001, 0, 5000);
    hs.moveToBoom(-61.105, -58.789, -4.199999999999818, 0, 5000);
    hs.moveToBoom(-1.518, -47.736, 0, 0, 5000);
  }
  /* 3 ring ladder red (start in middle of white line facing backwards towards the goal, across from laddar base)*/
  if (auton == 4)
  {
    hs.setPos(59, -12, 74);
    hs.drivePID(-36);
    goal.set_value(true);
    intake.move(125);
    xapi::delay(1000);
    hs.turnTo(13);
    intake.brake();
    hs.drivePID(20);
  }
  if (auton == 6)
  {
    hs.setPos(-59, 0, 90);
    hs.printCoords();
    intake.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
    intake.move(-88);
    pros::delay(500);
    intake.brake();
    hs.drivePID(5);
    hs.turnTo(210);
    hs.drivePID(-32);
    goal.set_value(true);
    pros::delay(500);
    intake.move(-88);

    hs.turnTo(90);

    hs.drivePID(34);
    pros::delay(400);
    hs.turnTo(-4);
    pros::delay(200);
    hs.drivePID(32);
    intake.move(50);
    pros::delay(300);
    intake.move(-88);
    hs.turnTo(-90);

    hs.drivePID(24);
    intake.move(50);
    pros::delay(300);
    intake.move(-88);
    hs.drivePID(16);
    intake.move(50);
    pros::delay(300);
    intake.move(-88);
    hs.turnTo(27);
    pros::delay(400);
    hs.drivePID(20, 1000.0);
    pros::delay(400);

    pros::delay(400);
    hs.turnTo(110);

    // score 1st goal
    pros::delay(400);
    intake.move(50);
    left_mg.move(-100);
    right_mg.move(-100);
    pros::delay(550);
    goal.set_value(false);
    left_mg.brake();
    right_mg.brake();
    pros::delay(300);
    hs.drivePID(15); // More

    // Travel
    intake.move(-86);
    hs.turnTo(0);
    hs.drivePID(-60);
    hs.turnTo(4);
    hs.drivePID(-42);
    goal.set_value(true);
    pros::delay(500);

    hs.turnTo(90);
    hs.drivePID(30);
    intake.move(50);
    pros::delay(300);
    intake.move(-88);

    hs.turnTo(180);
    pros::delay(200);
    hs.drivePID(26);
    pros::delay(500);

    hs.turnTo(270);

    hs.drivePID(24);
    pros::delay(500);
    hs.drivePID(16);
    intake.move(50);
    pros::delay(500);
    intake.move(-88);
    hs.turnTo(146);
    hs.drivePID(20, 1000.0);
    pros::delay(500);
    hs.turnTo(66);
    intake.move(50);
    left_mg.move(-100);
    right_mg.move(-100);
    pros::delay(550);
    left_mg.brake();
    right_mg.brake();
    goal.set_value(false);
    pros::delay(200);
    hs.drivePID(20); // More
    intake.brake();

    // 3rd Goal
    hs.turnTo(90); // 248-252
    // intake.move(-70);

    hs.drivePID(50);


    // rise.tare_position();
    // rise.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
    // rise.move_absolute(-95, 600);
    // pros::delay(280);
    // rise.brake();
    // hs.turnTo(180);
    // // hs.turnTo(250);
    // pros::delay(900);
    // rise.move(-80);
    // pros::delay(700);
    // rise.move(60);
    // pros::delay(500);
    // rise.brake();
    hs.turnTo(240);
    hs.drivePID(-60);
    pros::delay(500);
    // hs.turnTo(270);
    hs.drivePID(-35);

    goal.set_value(true);
    pros::delay(500);
    hs.turnTo(30);
    hs.drivePID(-20);
    hs.turnTo(-30);
    hs.drivePID(-50, 1400.0);
    goal.set_value(false);
    pros::delay(400);
    hs.drivePID(6);
    pros::delay(200);
    // 4th Goal
    hs.turnPID(300);
    intake.move(-50);
    hs.drivePID(20);
    intake.brake();
    pros::delay(500);
    hs.turnTo(180);
    hs.drivePID(-48);
    goal.set_value(true);
    intake.move(-80);
    pros::delay(800);
    intake.brake();
    hs.turnTo(196);
    hs.drivePID(-60);
    pros::delay(200);
    left_mg.move(-100);
    right_mg.move(-100);
    pros::delay(550);
    left_mg.brake();
    right_mg.brake();

    goal.set_value(false);
    // 5th
  }
}
// A task that only activates with a boolean, it keeps the rise at a constant position

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol()
{
  bool clamp = false;
  bool bigClamp = false;
  bool doink = false;
  bool autoClamp = false;
  bool ladyBrown = false;
  while (true)
  {
    // pros::lcd::print(0, "%d %d %d",
    //                  (pros::lcd::read_buttons() & LCD_BTN_LEFT) >> 2,
    //                  (pros::lcd::read_buttons() & LCD_BTN_CENTER) >> 1,
    //                  (pros::lcd::read_buttons() & LCD_BTN_RIGHT) >>
    //                      0); // Prints status of the emulated screen LCDs
    // Arcade control scheme
    int dir = master.get_analog(
        ANALOG_LEFT_Y); // Gets amount forward/backward from left joystick
    int turn = master.get_analog(
        ANALOG_RIGHT_X); // Gets the turn left/right from right joystick
    if (dir == 0 && turn == 0)
    {
      left_mg = 0;
      right_mg = 0;
    }
    if (master.get_digital(pros::E_CONTROLLER_DIGITAL_L1) == true)
    {
      // rise.move(57);
      // pros::delay(100);
      intake.move(-100);
    }
    else if (master.get_digital(pros::E_CONTROLLER_DIGITAL_L2) == true)
    {
      // rise.move(57);
      // pros::delay(100);
      intake.move(100);
    }
    else
    {
      intake.brake();
      // rise.brake();
    }

    // if (master.get_digital(pros::E_CONTROLLER_DIGITAL_UP) == true)
    // {
    //   // ladyBrown = !ladyBrown;
    //   rise.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
    //   rise.move_absolute(-78, 600);
    //   pros::delay(200);
    //   rise.brake();
    // }

    if ((master.get_digital(pros::E_CONTROLLER_DIGITAL_R1) == true) && !(master.get_digital(pros::E_CONTROLLER_DIGITAL_R2) == true))
    {
      rise.move(-70);
      pros::delay(150);
    }
    else if (master.get_digital(pros::E_CONTROLLER_DIGITAL_R2) == true && !(master.get_digital(pros::E_CONTROLLER_DIGITAL_R1) == true))
    {
      // if(rise.get_position() > 0)
      // {
      //   //idk
      // }else{
      // rise.move(70);
      // pros::delay(150);
      // }
      rise.move(70);
      pros::delay(150);
      rise.tare_position();
    }
    else if (master.get_digital(pros::E_CONTROLLER_DIGITAL_R1) == true && master.get_digital(pros::E_CONTROLLER_DIGITAL_R2) == true)
    {
      rise.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
      rise.move_absolute(-105, 600);
      pros::delay(280);
      rise.brake();
    }
    else
    {
      rise.brake();
    }

    // if (ladyBrown)
    // {
    //   rise.move_absolute(-78, 200);
    //   pros::delay(15);
    // }
    // else
    // {
    //   rise.brake();
    // }

    if (master.get_digital(pros::E_CONTROLLER_DIGITAL_X) == true)
    {
      doink = !doink;
      doinker.set_value(doink);
      pros::delay(300);
    }
    if (master.get_digital(pros::E_CONTROLLER_DIGITAL_B) == true)
    {
      clamp = !clamp;
      goal.set_value(clamp);
      pros::delay(300);
    }
    if (master.get_digital(pros::E_CONTROLLER_DIGITAL_Y) == true)
    {
      /* code */
      rise.tare_position();
    }

    // master.set_text(0,0,"Position: " + pos);
    // if (optical.get_hue() >= 60 && optical.get_hue() <= 70 && autoClamp == false)
    // {
    //   autoClamp = true;
    //   if (autoClamp)
    //   {
    //     pros::delay(500);
    //     goal.set_value(true);
    //   }
    // }
    // else if(!(optical.get_hue() >= 60 && optical.get_hue() <= 70))
    // {
    //   autoClamp = false;
    // }
    left_mg = (dir - (turn * .8));  // Sets left motor voltage
    right_mg = (dir + (turn * .8)); // Sets right motor voltage

    pros::delay(20); // Run for 20 ms then update
                     // hs.updateOdom();
  }
}