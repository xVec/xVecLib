
# Introduction

> :::note
> This Library is in pre-release! Be prepared for bugs, and please report them on the [repository bug page](https://codeberg.org/xvec/xveclib/issues/)

The xVex Everything code library (xVecLib) is a robust code library for both the Pros and Vex V5 API focused on usability, speed, and reliability while still being powerful. 

Found a bug? Report it to the [bug tracker](https://codeberg.org/xvec/xveclib/issues/)!

## Quick Start
First, clone the repository
```bash 
git clone https://codeberg.org/xVec/xVecLib.git
```
Now you can either start programming in the repository or manually installing it by dragging and dropping the include/xVecLib and the src/xVecLib folders. Once you got the project open, you can [configure your robot](<Robot Configuration>)! 
## Features
- In-depth API [documentation](https://xvec.codeberg.page). 
- [Path Generation ](<Path Generation>)Support
- Odometry
- Relative and Absolute Turn PID
- DrivePID
- Powerful MoveTo with backwards support
- Curved MoveTo (Boomerang)
- On-Screen Positioning GUI
- And more!!!
## Planned:
- [ ] Finish Comprehensive Docs including in-depth Guides 
- [ ] Reformat and speed up code 
- [ ] [Universal API](<Universal API>)
- [ ] Support the addition of addition tracking wheels/encoders
- [x] Finish [Path Generation](<Path generation>)
- [ ] Add Motion Profiling and general cubic path support
- [ ] Create automatic installation
- [ ] Create a more polished main screen for the brain with path viewing and autonomous selector
- [ ] Finish v2 Path generation
## Timeline:
```mermaid
timeline
    title xVecLib Timeline
    1.0.0 : Initial Release
    1.1.0 : Refractor code 
    1.2.0 : Rewrite all doc
    1.3.0 : Finish Universal API
    1.4.0 : Add extra encoder options
    1.5.0 : Motion Profiling
    1.6.0 : Automatic Install
    1.7.0 : Polished main screen
    1.8.0 : TBD
    1.9.0 : TBD
    2.0.0 : Add v2 Path Generation
```

## Questions? Features? Bugs? Let us know!
If you have any questions, features, or bugs please put them in the [bug tracker](https://codeberg.org/xVec/xVecLib/issues) 
		