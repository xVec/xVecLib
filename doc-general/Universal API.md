This library features an unfinished API called xAPI that allows the library to use certain classes and functions universally, no matter what platform or API the user is using. 

The files that will be affected by xAPI are listed below:
  draw.cpp
  control.cpp
  control.hpp
  robot.cpp
  robot.hpp
  structure.hpp

The methods/classes from the previous API(pros) are:

> [!NOTE] Note
> Functions with a link are implemented  in the pros support of xAPIFunctions with a link are implemented in the pros support of xAPI
 

draw.cpp:
```c
pros::screen::get_pen(); 
pros::screen::fill_rect
pros::screen::set_pen
pros::screen::draw_rect
pros::screen::fill_circle
pros::screen::draw_circle
pros::screen::draw_line
```
robot.cpp robot.hpp
```c
pros::MotorGroup
pros::Imu
```
structure.hpp
```c
pros::Motor
pros::adi::Encoder
pros::Rotation
```

control.cpp control.hpp
```c
pros::Mutex
pros::delay
pros::millis()
	```