xVecLib's robot configuration was originally inspired from [LemLib](https://github.com/LemLib/LemLib), but with various modifications to make it faster and more efficient. 

> [!attention] Sensor requirements and disclaimers
> Currently, the library only requires an Inertial(IMU) sensor to run. The library also currently doesn't support the addition of additional tracking wheels. This support will be added for [version 1.4.0](Todo##Timeline) If you want to contribute to this feature, use the odom struct
#### Steps:
1. Define left and right motor groups, and IMU sensor
2. Define linear(drive) and angular(turn) PID controllers
3. Define the robot with the above objects
4. Add final setup code to the initialize function
### Example Code:
```cpp
pros::MotorGroup left_mg({-1, -2, -3});
pros::MotorGroup right_mg({4, 5, 6});

pidSettings linearController(5,  // P
                     0,  // I
                     10, // D
                     3,  // Max I
                     1,  // small error range, in inches
                     3,  // large error range, in inches
                     5   // maximum acceleration (slew)
    );

pidSettings
    angularController(exactTP, // proportional gain (kP)
                      0,       // integral gain (kI)
                      6,       // derivative gain (kD)
                      3,       // anti windup
                      1.7,     // small error range, in degrees
                      4,       // large error range, in degrees
                      5        //(slew)
		    );


pros::IMU imu = pros::IMU(7);
double wheelDiameter = 3.5;
double gearRatio = 2.0;
robot hs = robot(&right_mg, &left_mg, &imu, linearController, angularController,
                 wheelDiameter, gearRatio);


void initialize()
{
    hs.setMoveTo(8, 127, 12);
    hs.setOdomAngle(y);
    left_mg.set_zero_position_all(0);
    right_mg.set_zero_position_all(0);
    while (imu.is_calibrating())
    {
      pros::delay(10);
    }
    pros::screen::set_eraser(0x000000);
    pros::screen::erase();
    pros::Task screenTask([&]()
    {
	    while (true) {
	        pros::screen::set_pen(0xFFFFFF);
	        //Print Robot Location to the Brain
	        pros::screen::print(pros::E_TEXT_MEDIUM, 2, "X: %f", ro.robotX);
	        pros::screen::print(pros::E_TEXT_MEDIUM, 3, "Y: %f", ro.robotY);
	        pros::screen::print(pros::E_TEXT_MEDIUM, 4, "Theta: %f", ro.Theta);
	        pros::screen::print(pros::E_TEXT_MEDIUM, 5, "Angle: %f", ro.robotAngle);
	        hs.updateOdom();
	        pros::delay(50);    
	    } 
    });
}
```