### Path Generation
 
> [!note] Note
> An additional format will be released around version 2.0.0 that integrates other commands from the library and allow custom code insertion into the path

Path generation allows users to plan out their autonomous and skills routes with a comprehensive interface. This library uses a custom format made for [path.jerryio](https://path.jerryio.com).

The format works by moving the point that has the least impact on the path to it's end control so the user has a better view of what the robot will actually do.  
 
> [!attention] Disclaimer
> The format generates code as close as possible to the goal path, but due to the nature of the boomerang method there is some curves that the robot cannot follow. Namely any lead over 1/-1. The format tries to find a nearby path that works by editing the heading, but sometimes this doesn't work. In these cases, the path will replace it with a 1 or -1 depending on what sign the lead previously was. 

 
