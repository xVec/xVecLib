Planned Features and Bug fixes


- [x] Fix broken Timer class/make a easy to use way of creating timers
- [x] Create a Boomerang Move To 
- [ ] Reformat and speed up code 
- [ ] Multi-platform API
- [ ] Create a more polished main screen for the brain with path viewing and autonomous selector
- [ ] Finish Comprehensive Docs including in-depth Guides 
- [x] Finish [Path Generation](https://xvec.codeberg.page/md_doc_general__path__generation.html)
- [ ] Finish v2 Path generation
- [ ] Support the addition of addition tracking wheels/encoders
- [ ] Add Motion Profiling and general cubic path support
- [ ] Create automatic installation

## Timeline:
```mermaid
timeline
    title xVecLib Timeline
    1.0.0 : Initial Release
    1.1.0 : Refractor code 
    1.2.0 : Rewrite all doc
    1.3.0 : Finish Universal API
    1.4.0 : Add extra encoder options
    1.5.0 : TBD

