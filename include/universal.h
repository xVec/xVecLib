/**
 * @file universal.h
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief This file allows you to switch from PROS API to VEX and vice-versa
vex requires you also declare the brain
 * @note The code for the actually switching is in the xVeclib/provex.hpp file
 * @version 1.0.0
 * @date 2024-10-22
 *
 * @copyright Copyright (c) 2024
 *
 */
#define PRO

//#define VEX

#ifdef PRO
#include "api.h"
#endif
#ifdef VEX
#include "vex.h"
extern vex::brain Brain;
#endif