/**
 * @file pid.h
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief This file serves as the header for PID
 * @version 1.0.0
 * @date 2024-10-22
 *
 * @copyright Copyright (c) 2024
 *
 */
#ifndef PID_H
#define PID_H
#include "api.h"
#include "draw.hpp"
#include "nonvex.hpp"
#include "pidValues.hpp"
#include "structure.hpp"
#include "provex.hpp"

#include <vector>

namespace xVecLib
{

  /**
   * @brief The control class is used to control individual motors with powerful
   * methods.
   *
   * @details This class provides methods for controlling the robot's movement,
   * including the PID control loop and moveTo.
   *
   * @param rightMotors The motors on the right side of the robot.
   * @param leftMotors The motors on the left side of the robot.
   * @param imu The Inertial Measurement Unit (IMU) that provides information
   * about the robot's orientation.
   * @param lateralSettings The PID values for controlling the robot's movement
   * laterally.
   * @param angularSetting The PID values for controlling the robot's movement
   * in a circular motion.
   */
  class control
  {
  private:
    // PID toggles
    /** @brief Toggle for using the drive PID */
    bool UseDrivePID = false;
    /** @brief Toggle for using the turn PID */
    bool UseTurnPID = false;
    /** @brief Toggle for starting the move method */
    bool startMove = false;
    // Various multi-use variables
    /** @brief The error in the turn PID */
    double turnError = 0;
    /** @brief The max distance that the robot can move without a new PID value
     * being set */
    double nearlyMaxDis = 0;
    /** @brief The power output of the motors */
    double power = 0;
    /** @brief The upper and lower limit of the power output */
    double limit = 0;
    /** @brief The iteration of the control loop */
    double iteration = 0;
    const int imax = std::numeric_limits<int>::max();
    /** @brief The iteration of the odometry */
    int odomIter = 0;

    // Move To
    /** @brief The previous distance of the robot */
    double prevDis;
    /** @brief The target angle of the robot */
    double endGoalAngle;
    /** @brief The parameters for the moveTo method */
    moveToParams moveParams;
    /** @brief The points for the moveTo method */
    std::vector<Point> movePointArray;
    /** @brief If the points have been edited */
    bool edited = false;
    /** @brief If the moveTo method should override the max move distance */
    bool overideMoveLimit = false;
    /** @brief The previous power of the drive motors */
    double prevDrivePower;
    /** @brief If the robot should fix the lateral movement */
    bool fixLateral = false;
    /** @brief If the robot should slow approach the target */
    bool slowApproach = true;
    /** @brief If the robot is moving backward */
    bool backward;
    /** @brief If the robot should not use the initial fix */
    bool noInitialFIx;
    /** @brief If the robot should use the new slow approach */
    bool useNewSlowApproach = false;

    /** @brief The lower bound of the power output */
    double lowThresh;
    /** @brief The maximum speed of the robot */
    double maxMoveSpeed = 127;
    /** @brief The minimum speed of the robot */
    double minMoveSpeed = 0;

    // Odom
    /** @brief The change in the left motor's position */
    double deltaL = 0;
    /** @brief The change in the right motor's position */
    double deltaR = 0;
    /** @brief The change in the distance traveled */
    double deltaD = 0;
    /** @brief The change in the robot's angle */
    double deltaTheta;
    /** @brief The current position of the left motor */
    double currentL;
    /** @brief The current position of the right motor */
    double currentR;
    /** @brief The previous position of the left motor */
    double previousL;
    /** @brief The previous position of the right motor */
    double previousR;
    /** @brief The current angle of the robot */
    double currentHead;
    /** @brief The previous angle of the robot */
    double previousHead;
    /** @brief The starting angle of the robot */
    double startingAngle;
    // Odom Settings
    /** @brief The gear ratio of the robot's drive */
    double gear = 0;
    /** @brief The diameter of the robot's wheels */
    double size = 0;
    /** @brief The increase in the distance traveled per motor rotation */
    double increase = 0;

    /** @brief One of the methods for the unfinished axis switching methods,
     * \returns If the current robotAxis is Y*/
    bool isYaxis();
    /** @brief One of the methods for the unfinished axis switching methods,
     * \returns The current robotAxis */
    bool whatAxis(cAxis ax);
    /** @brief A slower approach to calculating the power and turning the robot
     * \param pcts The percentage of the target that the robot is from
     * \param dis The distance to the target
     * \param drivePow The power of the drive motors */
    void slowCalc(double pcts, double dis, double drivePow);

    /** @brief Dynamically adjusts the PID values based on the robot's speed */
    void dynamicTuning(double type);
    /** @brief The moveTo method that uses the PID values */
    void dynamicMoveTo();

    /** @brief Checks if the motors are overheating */
    void checkOverheat();
    /**
     * @brief Runs a ramsete control loop to drive to X and Y
     * @note X and Y are in inches
     * @param X
     * @param Y
     */
    void ramsete(double X, double Y);

  protected:
    /**
     * @brief The PID values for the drive
     *
     * These values are set with xVecLib::PID::setDrivePID
     */
    pidValue drivePIDv;
    /**
     * @brief The PID values for the turn
     *
     * These values are set with xVecLib::PID::setTurnPID
     */
    pidValue turnPIDv;
    /**
     * @brief The PID values for the helper
     *
     * These values are set with xVecLib::PID::setHelperPID
     */
    pidValue helperPIDv;

    /**
     * @brief The PID values for the swing
     *
     * This value is beta and controls curve and swing PID
     */
    pidValue swingPID;

    /**
     * @brief The settings for the lateral PID
     *
     * These values are set with xVecLib::PID::setLateralSettings
     */
    pidSettings lateralSettings;
    /**
     * @brief The settings for the angular PID
     *
     * These values are set with xVecLib::PID::setAngularSettings
     */
    pidSettings angularSettings;
    /**
     * @brief The drawing object for the robot
     *
     * This is used to draw lines on the Field object
     */
    drawing drawer = drawing();
    /**
     * @brief The sensor object for the robot
     *
     * This is used to read the sensors on the robot
     */
    odom *additionOdom;

    /**
     * @brief The change in X value of the robot
     *
     * This is updated in the odometry loop
     */
    double deltaX = 0;
    /**
     * @brief The change in Y value of the robot
     *
     * This is updated in the odometry loop
     */
    double deltaY = 0;
    /**
     * @brief A pointer to the right Motors
     */
    xapi::MotorGroup *rightMotors;
    /**
     * @brief A pointer to the top left Motors
     */
    xapi::MotorGroup *leftMotors;
    /**
     * @brief A pointer to the xapi::IMU sensor on the robot
     *
     * This is required for odometry
     */
    xapi::IMU *imu;
    /**
     * @brief A mutex for the moveTo method
     *
     * This is used to prevent multiple threads from running the moveTo method
     * simultaneously
     */
    xapi::Mutex move;

  public:
    /**
     * @brief Constructor for the control class
     *
     * @param _rightMotors The motors on the right side of the robot
     * @param _leftMotors The motors on the left side of the robot
     * @param _imu The Inertial Measurement Unit (IMU) that provides information
     * about the robot's orientation
     * @param linearSetting The PID values for controlling the robot's movement
     * laterally
     * @param angularSetting The PID values for controlling the robot's movement
     * in a circular motion
     * @param sensors The sensors that the robot uses for odometry
     */
    control(xapi::MotorGroup *_rightMotors, xapi::MotorGroup *_leftMotors,
            xapi::IMU *_imu, pidSettings linearSetting,
            pidSettings angularSetting, double wheelDiameter, double gearRatio);
    /**
     * @brief The gear ratio of the robot's drive
     *
     * This is used to calculate the robot's speed
     */
    double gearRatio = 1;
    /**
     * @brief The diameter of the robot's wheels
     *
     * This is used to calculate the robot's speed
     */
    double wheelDiameter = 1;

    /**
     * @brief The pose of the robot
     *
     * This is updated in the odometry loop
     */
    Point odomPose = Point(0, 0, 0);
    /**
     * @brief The speed of the robot
     *
     * This is updated in the odometry loop
     */
    Point odomSpeed = Point(0, 0, 0);
    /**
     * @brief The local speed of the robot
     *
     * This is updated in the odometry loop
     */
    Point odomLocalSpeed = Point(0, 0, 0);
    /**
     * @brief Set the Drive PID object
     *
     * Sets the Drive PID object with the given values.
     *
     * @param pidVal The values to set
     */
    void setDrivePID(pidValue pidVal);
    /**
     * @brief Get the Drive PID object
     *
     * Gets the current Drive PID object.
     *
     * @return pidValue
     */
    pidValue getDrivePID();
    /**
     * @brief Set the Turn PID object
     *
     * Sets the Turn PID object with the given values.
     *
     * @param pidVal
     */
    void setTurnPID(pidValue pidVal);
    /**
     * @brief Get the Turn PID object
     *
     * Gets the current Turn PID object.
     *
     * @return pidValue
     */
    pidValue getTurnPID();
    /**
     * @brief Set the Helper PID object
     *
     * Sets the Helper PID object with the given values.
     *
     * @param pidVal
     */
    void setHelperPID(pidValue pidVal);
    /**
     * @brief Get the Helper PID object
     *
     * Gets the current Helper PID object.
     *
     * @return pidValue
     */
    pidValue getHelperPID();
    /**
     * @brief Set the Swing PID object
     *
     * Sets the Swing PID object with the given values.
     *
     * @param pidVal
     */
    void setSwingPID(pidValue pidVal);

    /**
     * @brief Get the Swing PID object
     *
     * Gets the current Swing PID object.
     *
     * @return pidValue
     */
    pidValue getSwingPID();
    /**
     * @brief Drive to a position using PID
     *
     * Drives the robot to a position using the PID control loop. The PID values
     * set using setSwingPID() are used to control the movement. The target
     * parameter is the position to move to. The limit parameter is the amount of
     * time in milliseconds to allow for the movement. If the limit is set to 0,
     * then the movement will continue until the target is reached.
     *
     * @param target The position to move to
     * @param limit The amount of time to allow for the movement in seconds
     */
    void drivePID(double target, double limit = 0);

    /**
     * @brief Drive to a position in a curve using PID
     * @author Laynce Lim 1082 member
     * @param targetX The target X position
     * @param targetY The target Y position
     * @param goingForward If it's going forward
     * @param slowdown  IDK
     */

    void curvePID(double targetX, double targetY,
                  bool goingForward, bool slowdown);
    /**
     * @brief Moves the robot to the specified coordinates.
     *
     * @param x double - The target x-coordinate.
     * @param y double - The target y-coordinate.
     * @param goingForward bool - True if the robot should move forward, false if it should move backward.
     */
    void simpleMoveTo(double x, double y, bool goingForward = true, bool initialTurn = true);

    /**
     * @brief Moves the robot to the specified coordinates.
     *
     * @param point Point - The target coordinates.
     * @param goingForward bool - True if the robot should move forward, false if it should move backward.
     */
    void moveToPoint(Point point, bool goingForward);

    /**
     * @brief Get the current pose of the robot
     *
     * Gets the current pose of the robot. The pose is returned as a Point
     * object containing the X and Y coordinates of the robot. If radians is true,
     * the angle is returned in radians, otherwise degrees. If standardPos is true,
     * the X and Y coordinates are converted to standard coordinates.
     *
     * @param radians True means return the angle in radians, false means degrees
     * @param standardPos True means convert the coordinates to standard coordinates
     * @return Point containing the X and Y coordinates and the angle of the robot
     */
    Point getPose(bool radians = false, bool standardPos = false);

    /**
     * @brief Begins the drive PID control loop and fixes the angle to whay it
     * was before the robot moved
     *
     * @param target The target value for the drive PID control loop in inches.
     * @param fixAngle True means the robot will fix the Angle, false will not
     */
    void drivePID(double target, bool fixAngle);

    /**
     * @brief Begins the turn PID control loop.
     * @param target The target value for the turn PID control loop in degrees.
     */
    void turnPID(double target);

    /**
     * @brief Uses PID to turn to a rotation relative to the robot's starting
     * rotation
     * @param targetRotation The rotation to turn to
     */
    void turnTo(double targetRotation);

    /**
     * @brief Uses the xapi::IMU sensor and the motors to calculate the change
     * in angle
     * @attention This is one of the most important methods and must be executed
     * for any method that requires drawing
     *
     */
    void updateOdom(void);

    // void updateOdom(bool);
    /**
     * @brief Drives forward an amount of x and y in a linear form
     * @note In otherwords, It drives forward the amount of X, forward the
     * amount of Y
     * @param DriveTargetX
     * @param DriveTargetY
     */
    void DriveFront(double DriveTargetX, double DriveTargetY);
    /**
     * @brief Drives reverse an amount of x and y in a linear form
     * @note In otherwords, It drives backward the amount of X, forward the
     * amount of Y
     * @param DriveTargetX
     * @param DriveTargetY
     */
    void DriveBack(double DriveTargetX, double DriveTargetY);
    /**
     * @brief Set the Odom Angle object
     *
     * @param odomAxis
     */
    void setOdomAngle(cAxis odomAxis);

    /**
     * @brief Adds an additional tracking wheel, rotaion sensor, or motor to odometry
     *
     * @param newOdom
     */
    void addOdom(odom *newOdom);

    /**
     * @brief Runs an advanced PID control loop
     *
     * @param driveX
     * @param driveY
     */
    void helper(double driveX, double driveY);

    /**
     * @brief A method that drives to x and y
     * @attention Of all the methods so far created, this is both the most
     * tested and accurate
     * @note x and y are in inches
     *
     * @param x
     * @param y
     * @param timeOut Defaults to 0
     */
    void moveTo(double x, double y, double timeOut = 0);

    /**
     * @brief A method that drives to x and y using the boomerang controller
     *
     * @param x The x coordinate of the target
     * @param y The y coordinate of the target
     * @param theta The angle of the target
     * @param lead The lead of the boomerang controller
     * @param timeOut The time to wait for the robot to reach the target
     */
    void moveToBoom(double x, double y, double theta, double lead,
                    double timeOut = 0);

    /**
     * @brief A method that drives to x and y using the boomerang controller
     *
     * @param x A pointer to the x coordinate of the target
     * @param y A pointer to the y coordinate of the target
     * @param theta A pointer to the angle of the target
     * @param lead A pointer to the lead of the boomerang controller
     * @param timeOutr A pointer to the time to wait for the robot to reach the target
     */
    void moveToBoom(double *xr, double *yr, double *thetar, double *leadr,
                    double *timeOutr = nullptr);

    /**
     * @brief A method that drives to x and y but overides the behavior of
     * various variables
     * @attention Of all the methods so far created, this is both the most
     * tested and accurate
     * @note x and y are in inches
     *
     * @param x
     * @param y
     * @param limits
     * @param params
     */
    void moveTo(double x, double y, double limits,
                moveToParams params);

    /**
     * @brief A method that defines the path for the xVecLib::PID::moveToPath
     * method
     * @param Point NewPoint
     */
    void addPoint(Point NewPoint);
    /**
     * @brief An unfinished method that runs the xVecLib::PID::moveTo method
     * along a predefined path
     * @note The path is defined by points inserted using the
     * xVecLib::PID::addPoint method
     */
    void moveToPath();

    /**
     * @brief Sets the variables of the moveTo method.
     *
     * @param maxDis The distance that the robot will start fixing turns \note
     * The maxDis variable is poorly coded in general and WILL be redone in a
     * future release
     * @param maxPower The maxPower of the robot
     *
     * @param lowThresh The smallest distance that it can stop, defaults to 12
     *    */
    void setMoveTo(double maxDis, double maxPower, double lowThresh);
    /**
     * @brief Prints the current x, y, and heading of the robot
     */
    void printCoords();

    /**
     * @brief Clears the path of points used in the moveToPath method
     */
    void clearPath(void);

    /**
     * @brief Makes the robot swing to the right
     * @param angle The angle to swing
     * @param oppositeSpeed The speed of the opposite side of the robot
     */
    void right_swing(float angle, float oppositeSpeed);

    /**
     * @brief Makes the robot swing to the left
     * @param angle The angle to swing
     * @param oppositeSpeed The speed of the opposite side of the robot
     */
    void left_swing(float angle, float oppositeSpeed);
    /**
     * @brief Sets the robots position
     * @param x
     * @param y
     * @param theta
     */
    void setPos(double x, double y, double theta);

    /**
     * @brief Moves the cursor
     * @param x
     * @param y
     */
    void moveCursor(int x, int y);
  };

} // namespace xVecLib
#endif
