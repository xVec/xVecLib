#ifndef STRUCTURE_H
#define STRUCTURE_H

#include "nonvex.hpp"
#include "pros/adi.hpp"
#include "pros/rotation.hpp"
#include "provex.hpp"

namespace xVecLib
{
  template <class T>
  constexpr const T &clamp(const T &v, const T &lo, const T &hi)
  {
    return v < lo ? lo : hi < v ? hi
                                : v;
  }

  /**
   * @brief An enum that contains the 2 possible axis, X and Y
   *
   */
  enum cAxis
  {
    x,
    y
  };

  /**
   * @brief A struct for odom devices
   * This includes encoders, rotaion sensors, motor groups, and motors
   */
  struct odom
  {
    odom(double _gearRatio, double _wheelDiameter, xapi::Motor *_motor)
        : gearRatio(_gearRatio), wheelDiameter(_wheelDiameter), motor(_motor) {};

    odom(xapi::Encoder *encoder, float wheelDiameter, float distance,
         float gearRatio = 1)
        : gearRatio(gearRatio), wheelDiameter(wheelDiameter), encoder(encoder) {};
    odom() {};
    /**
     * @brief Create a new tracking wheel
     *
     * @param encoder the v5 rotation sensor to use
     * @param wheelDiameter the diameter of the wheel
     * @param distance distance between the tracking wheel and the center of
     * rotation in inches
     * @param gearRatio gear ratio of the tracking wheel, defaults to 1
     */
    odom(xapi::Rotation *rotation, float wheelDiameter, float distance,
         float gearRatio = 1)
        : gearRatio(gearRatio), wheelDiameter(wheelDiameter),
          rotation(rotation) {};
    /**
     * @brief Create a new tracking wheel
     *
     * @param motors the motor group to use
     * @param wheelDiameter the diameter of the wheel
     * @param gearRatio the gear ratio of the drivetrain, defaults to 1
     */
    odom(xapi::MotorGroup *motors, float wheelDiameter, float gearRatios = 1)
        : gearRatio(gearRatios), wheelDiameter(wheelDiameter), motors(motors) {};
    
    void reset()
    {
      if (this->encoder != nullptr)
        this->encoder->reset();
      if (this->rotation != nullptr)
        this->rotation->reset_position();
      if (this->motors != nullptr)
        this->motors->tare_position();
    }

    double getDistanceTraveled()
    {
      if (this->encoder != nullptr)
      {
        return (float(this->encoder->get_value()) * this->wheelDiameter * M_PI /
                360) /
               this->gearRatio;
      }
      else if (this->rotation != nullptr)
      {
        return (float(this->rotation->get_position()) * this->wheelDiameter *
                M_PI / 36000) /
               this->gearRatio;
      }
      else if (this->motors != nullptr)
      {

        std::vector<double> positions = this->motors->get_position_all();

        return calcInchesPerTick(avg(positions), wheelDiameter, gearRatio);
      }
      else if (this->motor != nullptr)
      {
        return (float(this->motor->get_position()) * this->wheelDiameter * M_PI /
                360) /
               this->gearRatio;
      }
      else
      {
        return 0;
      }
    }
    /**
     * @brief Get the offset of the tracking wheel from the center of rotation
     *
     * @return float offset in inches
     */
    float getOffset() { return this->distance; }

    /**
     * @brief Get the type of tracking wheel
     *
     * @return int - 1 if motor group, 0 otherwise
     */
    int getType()
    {
      if (this->motors != nullptr)
        return 1;
      return 0;
    }
    xapi::MotorGroup *motors = nullptr;
    xapi::Encoder *encoder = nullptr;
    xapi::Rotation *rotation = nullptr;
    xapi::Motor *motor = nullptr;

  private:
    float distance;
    double wheelDiameter;
    float gearRatio = 1;
  };

  /**
   * @brief struct containing the moveTo params
   *
   *
   */
  struct moveToParams
  {
    /// Boolean representing if the robot should slow down as approachig the
    /// target, as apposed of a sudden stop
    bool noSlowApproach;
    /// Enables the endAngle fix, if enabled the endAngle parameter must also be
    /// set
    bool doEndAngle;
    /// Disables the initial turn fix. This speeds up the method but may not be as
    /// accurate
    bool noInitialFIx;
    /// Drives and turns reverse from what It would. In otherwords, it goes
    /// backwards to the point
    bool backwards;
    // bool fixBadPos = false;
    /// Represents the max speed of the robot
    double maxSpeed;
    /// Represents the min speed of the robot
    double minSpeed;
    /// Paired with the doEndAngle, represents the endAngle in degrees
    double endAngle;
    /// A boolean representing if the robo-t should use the new slow approach
    bool useNewSlowApproach;
  };

  struct pidSettings
  {
    /**
     * The constants are stored in a struct so that they can be easily passed to
     * the control/robot classes
     *
     * @param kP Proportional
     * @param kI Integral
     * @param kD Derivative
     * @param windupRange the range of the windup, used as max I
     * @param smallError the error at which the robot begins to stop
     * @param largeError the error at which the robot begins to slow
     * @param slew the slew rate of the robot's drive
     */
    pidSettings(float kP, float kI, float kD, float windupRange,
                float smallError,
                float largeError, float slew)
        : kP(kP), kI(kI), kD(kD), windupRange(windupRange),
          smallError(smallError),
          largeError(largeError),
          slew(slew) {}
    /// Proportional
    float kP;
    float kI;
    float kD;
    float windupRange;
    float smallError;
    float smallErrorTimeout;
    float largeError;
    float largeErrorTimeout;
    float slew;
  };

} // namespace xVecLib

#endif