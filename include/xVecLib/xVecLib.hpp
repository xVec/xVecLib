/**
 * @file xVecLib.h
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief This is the main file to include.
 * It includes all of the library headers
 * @version 0.1.0
 * @date 2024-01-10
 *
 *
 */
#include "structure.hpp"
#include "robot.hpp"

#ifndef roboStuff
#define roboStuff
/**
 * @brief The main Namespace for the Library
 *
 */
namespace xVecLib {}
enum roboColor { rBlue, rRed };
/**
 * @brief The robotAccess struct allows PID and other code in xVecLib to gain
 * access to the robot variables
 */
struct robotAccess {
private:
  /**
   * @brief A simple struct to represent a coordinate
   * @note This is just a simple struct, and is not part of the library
   */
  struct coordinate {
    /** The X coordinate */
    double x;
    /** The Y coordinate */
    double y;
  };

public:
  /**
   * @brief Constructor that sets the position to 0,0
   */
  robotAccess()
      : robotX(0), robotY(0), robotAngle(0), Theta(0),
        robotAxis(xVecLib::cAxis::y) {}

  /**
   * @brief Constructor that sets the position to the given coordinates
   * @param posX The X coordinate
   * @param posY The Y coordinate
   */
  robotAccess(double posX, double posY) {
    robotX = posX;
    robotY = posY;
  }

  /**
   * @brief Constructor that sets the position to the given coordinates
   * @param coord The coordinate
   */
  robotAccess(coordinate coord) {
    robotX = coord.x;
    robotY = coord.y;
  }

  /**
   * @brief Sets the position to the given coordinates
   * @param posX The X coordinate
   * @param posY The Y coordinate
   */
  void set(double posX, double posY) {
    robotX = posX;
    robotY = posY;
  }

  /**
   * @brief Sets the position to the given coordinates
   * @param coord The coordinate
   */
  void set(coordinate coord) {
    robotX = coord.x;
    robotY = coord.y;
  }

  /**
   * @brief Gets the position as a coordinate
   * @return The coordinate
   */
  coordinate get() { return (coordinate){robotX, robotY}; }
  /** Represents the current robot X */
  double robotX = 0;
  /** Represents the current robot Y*/
  double robotY = 0;
  /** Represents the current robot Angle in degrees*/
  double robotAngle = 0;
  /** Represents the current robot Angle in radians*/
  double Theta = 0;
  /**Represents the Robot Color. Only used in the drawing class and will be
   * combined with the robot class*/
  roboColor robotColor;
  /** Represents the starting axis of the robot. Only set it to Y as X isn't
   * finished yet. See xVecLib::PID::setOdomAngle*/
  xVecLib::cAxis robotAxis;
  xVecLib::Point getPoint() {
    return xVecLib::Point(robotX, robotY, robotAngle);
  }
};
/**
 * @brief void setX(double Num) { robotX = Num; }
  void setY(double Num) { robotY = Num; }
  void setAngle(double Ang) { robotAngle = Ang; }
  double getX() { return robotX; }
  double getY() { return robotY; }
  double getAngle() { return robotAngle; }
  xVecLib::Point getPoint() {
    return xVecLib::Point(robotX, robotY, robotAngle);
  }

private:
  bool isSettled = false;
  bool seemsDone = false;
  bool seemsIsDone = false;

public:
  void setSeemsDone(bool done) { seemsDone = done; }
  bool getSeemsDone() { return seemsDone; }
  bool getState() { return isSettled; }
  void setState(bool state) { isSettled = state; }
  bool getSeemsIsDone() { return seemsIsDone; }
  void setSeemsIsDone(bool done) { seemsIsDone = done; }
  void updateState(bool done, xVecLib::Point goal) {
    xVecLib::Point pose = {robotX, robotY, robotAngle};
    xVecLib::Point dif = (goal - pose);
    if (dif.x < 6 && dif.y < 6 && dif.x > -6 && dif.y > -6 && done) {
      seemsDone = true;
      if (seemsIsDone) {
        isSettled = true;
      }
    }
  }
 *
 */
/**
 * @brief ro is the global instance of the robotAccess struct
 * @attention It is completely integrated into all aspects of the library and
 * cannot be removed or changed without changing all instances of it.
 * @note In the future, the ability to define your own instance of robotAccess
 * may be added to the library
 *
 */
extern robotAccess ro;

#endif

/**
 * @brief
 xveclib/
├── include/
│   └── xveclib/
│       ├── display.hpp  // Screen drawing
│       ├── logging.hpp  // Data logging
│       ├── state_tracking.hpp  // Robot state tracking
│       ├── pid.hpp  // PID controller
│       ├── control.hpp  // High-level control loops
│       ├── utilities.hpp  // Non-robot specific utilities
│       ├── robot_structs.hpp  // Robot related structs
│       ├── robot_enums.hpp  // Robot related enums
│       └── pid_methods.hpp  // Methods using PID
└── src/
    └── xveclib/
        ├── display.cpp
        ├── logging.cpp
        ├── state_tracking.cpp
        ├── pid.cpp
        ├── control.cpp
        ├── utilities.cpp
        ├── robot_structs.cpp
        ├── robot_enums.cpp
        └── pid_methods.cpp
 *
 */