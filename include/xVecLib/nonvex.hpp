/**
 * @file nonvex.hpp
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief Various non-vex code
 * @version 1.0.0
 * @date 2024-10-22
 *
 * @copyright Copyright Cavaire(c) 2024 with code from LemLib
 *
 */
#ifndef UTIL_H
#define UTIL_H

#include <cstdio>
#include <iostream>
#include <math.h>
#include <vector>

namespace xVecLib {
/**
 * @brief A point. Used in all path tracking methods
 *
 */
class Point {
private:
  double pX, pY, pAngle, pVelocity, pCurve;

public:
  /** @brief x value*/
  double x;
  /** @brief y value*/
  double y;
  /** @brief theta value*/
  double theta;

  /**
   * @brief Create a new Point
   *
   * @param X component
   * @param Y component
   * @param theta heading. Defaults to 0
   */
  Point(double X, double Y, double theta = 0) {
    x = X, y = Y, pAngle = theta;
    pCurve = 0, pVelocity = 0;
  }

  double getX() { return x; }
  double getY() { return y; }
  double getAngle() { return theta; }
  void setX(double Num) { x = Num; }
  void setY(double Num) { y = Num; }
  void setAngle(double Ang) { theta = Ang; }
  double getCurve() { return pCurve; }
  double getVelocity() { return pVelocity; }
  void setCurve(double curve) { pCurve = curve; }
  void setVelocity(double vel) { pVelocity = vel; }
  void ExportPoint() {
    std::cout << "(" << pX << ", " << pY << ")";
    printf("(%f, %f, %f)", pX, pY, theta);
  }
  std::string toString() {
    return "(" + std::to_string(pX) + ", " + std::to_string(pY) + ", " +
           std::to_string(theta) + ")";
  }

  /**
   * @brief Add a Point to this Point
   *
   * @param other other Point
   * @return Point
   */
  Point operator+(const Point &other);
  /**
   * @brief Subtract a Point from this Point
   *
   * @param other other Point
   * @return Point
   */
  Point operator-(const Point &other);
  /**
   * @brief Multiply a Point by this Point
   *
   * @param other other Point
   * @return Point
   */
  double operator*(const Point &other);
  /**
   * @brief Multiply a Point by a double
   *
   * @param other double
   * @return Point
   */
  Point operator*(const double &other);
  /**
   * @brief Divide a Point by a double
   *
   * @param other double
   * @return Point
   */
  Point operator/(const double &other);
  /**
   * @brief Linearly interpolate between two Points
   *
   * @param other the other Point
   * @param t t value
   * @return Point
   */
  Point lerp(Point other, double t);
  /**
   * @brief Get the distance between two Points
   *
   * @param other the other Point
   * @return double
   */
  double distance(Point other) const;
  /**
   * @brief Get the angle between two Points
   *
   * @param other the other Point
   * @return double in radians
   */
  double angle(Point other) const;
  /**
   * @brief Rotate a Point by an angle
   *
   * @param angle angle in radians
   * @return Point
   */
  Point rotate(double angle);
};

/**
 * @brief Calculates the inches per one tick of the Motor
 * @note Ticks represent what you are tracking (degrees)
 *
 * @param current
 * @return double
 */
double calcInchesPerTick(double current, double size, double ratio);
/**
 * @brief Converts pos from inches to tiles
 *
 * @param pos
 * @return pos in tiles
 */
double getTilePos(double pos);
/**
 * @brief Converts tile from tiles to inches
 *
 * @param tile
 * @return double
 */
double getPosTile(double tile);
/**
 * @brief Returns 0 if num is equal to 0, 1 if it's greater, -1 if it's less
 *
 * @param num
 * @return double
 */
double checkSign(double num);
/**
 * @brief Converts num from degrees to radians
 *
 * @param num
 * @return The value in radians of num
 */
double convertRadians(double num);
/**
 * @brief Converts num from radians to degrees
 *
 * @param num
 * @return The value in degrees of num
 */
double convertDegrees(double num);
/**
 * @brief Slew rate limiter
 *
 * @param target target value
 * @param current current value
 * @param maxChange maximum change. No maximum if set to 0
 * @return double - the limited value
 */
double slew(double target, double current, double maxChange);

/**
 * @brief Convert radians to degrees
 *
 * @param rad radians
 * @return double degrees
 */
constexpr double radToDeg(double rad) { return rad * 180 / M_PI; }

/**
 * @brief Convert degrees to radians
 *
 * @param deg degrees
 * @return double radians
 */
constexpr double degToRad(double deg) { return deg * M_PI / 180; }

/**
 * @brief Calculate the error between 2 angles. Useful when calculating the
 * error between 2 headings
 *
 * @param angle1
 * @param angle2
 * @param radians true if angle is in radians, false if not. False by default
 * @return double wrapped angle
 */
double angleError(double angle1, double angle2, bool radians = true);

/**
 * @brief Return the sign of a number
 *
 * @param x the number to get the sign of
 * @return int - -1 if negative, 1 if positive
 */
template <typename T> constexpr T sgn(T value) { return value < 0 ? -1 : 1; }

/**
 * @brief Return the average of a vector of numbers
 *
 * @param values
 * @return double
 */
double avg(std::vector<double> values);

/**
 * @brief Exponential moving average
 *
 * @param current current measurement
 * @param previous previous output
 * @param smooth smoothing factor (0-1). 1 means no smoothing, 0 means no change
 * @return double - the smoothed output
 */
double ema(double current, double previous, double smooth);

/**
 * @brief Get the signed curvature of a circle that intersects the first Point
 * and the second Point
 *
 * @note The circle will be tangent to the theta value of the first Point
 * @note The curvature is signed. Positive curvature means the circle is going
 * clockwise, negative means counter-clockwise
 * @note Theta has to be in radians and in standard form. That means 0 is right
 * and increases counter-clockwise
 *
 * @param Point the first Point
 * @param other the second Point
 * @return double curvature
 */
double getCurvature(Point first, Point other);

} // namespace xVecLib
#endif