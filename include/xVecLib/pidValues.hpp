/**
 * @file pidValues.hpp
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief This contains the pidValue struct that holds PID values
 * @details The struct also can calculate the output of the PID loop
 * @version 1.0.0
 * @date 2024-10-22
 *
 * @copyright Copyright (c) 2024
 *
 */
#ifndef PID_VAL_H
#define PID_VAL_H
/**
 * @brief Struct to hold PID values
 *
 * @details This struct holds PID values for a single PID controller. It also
 *          has some other values that are used in the PID loop, such as the
 *          output of the previous iteration and the integral of the error.
 */
struct pidValue {
  /// @brief Constructor
  pidValue() : P(0), I(0), D(0), goal(0), timeOut(0), windupRange(0),
               signFlipReset(false), integral(0), prevError(0), itError(0),
               traveled(0) {}

  /// @brief Constructor with all values
  ///
  /// @param vgoal The goal value
  /// @param vtimeOut The time out
  /// @param vP The proportional value
  /// @param vI The integral value
  /// @param vD The derivative value
  pidValue(double vgoal, double vtimeOut, double vP, double vI, double vD)
      : goal(vgoal), timeOut(vtimeOut), P(vP), I(vI), D(vD), windupRange(0),
        signFlipReset(false), integral(0), prevError(0), itError(0),
        traveled(0) {}

  /// @brief Constructor with windup range and sign flip reset
  ///
  /// @param kP The proportional value
  /// @param kI The integral value
  /// @param kD The derivative value
  /// @param windupRange The maximum integral value
  /// @param signFlipReset Reset integral if sign of error changes
  pidValue(double kP, double kI, double kD, double windupRange = 0,
           bool signFlipReset = false)
      : P(kP), I(kI), D(kD), windupRange(windupRange), signFlipReset(signFlipReset),
        integral(0), prevError(0), itError(0), traveled(0) {}

  /// @brief The proportional value
  double P;
  /// @brief The integral value
  double I;
  /// @brief The derivative value
  double D;
  /// @brief The goal value
  float goal;
  /// @brief The time out
  float timeOut;

  /// @brief The maximum integral value
  double windupRange;
  /// @brief Reset integral if sign of error changes
  bool signFlipReset;

  /// @brief The integral of the error
  double integral;
  /// @brief The error of the previous iteration
  double prevError;
  /// @brief The integral of the error divided by the goal
  double itError;
  /// @brief The traveled distance
  double traveled;

  /**
   * @brief Update the PID loop
   *
   * @param error The error value
   * @return The output of the PID loop
   */
  double update(const double error) {
    // calculate integral
    integral += error;
    if (xVecLib::sgn(error) != xVecLib::sgn((prevError)))
      integral = 0;
    if (fabs(error) > windupRange)
      integral = 0;

    // calculate derivative
    const double derivative = error - prevError;
    prevError = error;

    // calculate output
    itError = integral / goal;
    return error * P + integral * I + derivative * D;
  }

  /// @brief Reset the integral to zero
  void reset() {
    integral = 0;
    prevError = 0;
  }

  /**
   * @brief Check if the PID loop is settled
   *
   * @return True if the loop is settled
   */
  bool is_settled() {
    if (traveled >= goal || itError < 1) {
      return true;
    } else {
      return false;
    }
  }
};
#endif
