/**
 * @file track.h
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief Contains the drawing class which draws on to the brain.
 * @version 1.0.0
 * @date 2024-10-22
 * @copyright Copyright (c) 2024
 *
 */

#ifndef DRAW_H
#define DRAW_H
namespace xVecLib {

class drawing {
private:
  double yfieldvalue;
  double xfieldvalue;
  double prevX = 0;
  double prevY = 0;

public:
  drawing(){};

  /**
   * @brief Draws a tile on the brain. Used in the
   * xVecLib::drawing::drawField();
   *
   * @param x The x value of the tile. Ranges from 1-6
   * @param y The y value of the tile. Ranges from 1-6
   */
  void drawTile(double x, double y);
  /**
   * @brief Draws a field on the robot's brain
   *
   * This function draws a field on the robot's brain using the data from
   * xVecLib::drawing::xfieldvalue and yfieldvalue
   */
  void drawField(void);
  /**
   * @brief A screen that uses xVecLib to display internal data
   *
   * This function displays a screen that uses xVecLib to display internal
   * data about the robot.
   */
  void xVecLibScreen(void);
};

} // namespace xVecLib

#endif
