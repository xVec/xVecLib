/**
 * @file robot.h
 * @brief Contains the robot class
 * @version 0.2.0
 * @date 2024-01-10
 * @author Cavaire (cavaire3d@gmail.com)
 *
 * @details This file defines the `robot` class, which allows basic control of
 * the robot using the `pid` class.
 *
 * @remark Since most of the methods are self-explanatory, comments are not
 * provided for each method. However, you can refer to the method definitions
 * for more information.
 */

#ifndef ROBOT_H
#define ROBOT_H

#include "nonvex.hpp"
#include "control.hpp"


namespace xVecLib {
/**
 * @brief A class to allow basic control of the robot using the pid class
 *
 * @details This class provides methods for controlling the robot's movement,
 * scoring mechanism, and more.
 */
class robot : public control {
private:
  // Conversion factor for grid to millimeters
  const double milimetersPerGrid = 600;
  // Conversion factor for grid to inches
  const double inchesPerGrid = 24;

public:
  /**
   * @brief Constructor for the `robot` class
   *
   * @param _rightMotors The motors on the right side of the robot
   * @param _leftMotors The motors on the left side of the robot
   * @param _imu The IMU that provides information about the robot's orientation
   * @param linearSetting The linear PID values
   * @param angularSetting The angular PID values
   * @param wheelDiameter The diameter of the robot's wheel
   * @param gearRatio The gear ratio of the robot
   * 
   */
  robot(xapi::MotorGroup *_rightMotors, xapi::MotorGroup *_leftMotors, xapi::IMU *_imu, pidSettings linearSetting,
        pidSettings angularSetting, double wheelDiameter, double gearRatio);

      // Movement methods

      /// @brief Drives the Robot forward tiles
      /// @param tiles The amount of tiles the robot should go.
      void driveForwardTiles(double tiles);

  /// @brief
  /// @param inches The amount of inches the robot should go.
  /// @note This uses the PID::DriveFront method instead of drivePID
  void driveForwardInches(double inches);
  /**
   * Drive the robot in reverse for a given number of tiles.
   *
   * @param tiles - The number of tiles to drive in reverse.
   */
  void driveReverseTiles(double tiles);
  /**
   * Drive the robot in reverse for a given number of inches.
   *
   * @param inches - The number of inches to drive in reverse.
   */
  void driveReverseInches(double inches);
  /**
   * Turn the robot to the right by the specified degrees.
   *
   * @param degree the degrees to turn the robot to the right
   */
  void turnRight(double degree);
  /**
   * Turn the robot to the left by the specified degrees.
   *
   * @param degree the degrees to turn the robot to the left
   */
  void turnLeft(double degree);
  
  /**
   * @brief Runs the moveTo method, but converts tileX and tileY to the amount
   * of inches they would represent
   *
   * @param tileX The x value of the tile
   * @param tileY The y value of the tile
   */
  void moveToTile(double tileX, double tileY);

  /**
   * @brief Adds a point in the form of a point object to the moveToPath
   *
   * @param tile
   */
  void addTilePoint(Point tile);
  /**
   * @brief Adds a point in standard (x,y) to moveToPath
   *
   * @param tileX
   * @param tileY
   */
  void addTilePoint(double tileX, double tileY);
  /**
   * @brief Adds a point in the (y, x) form to moveToPath
   * @note This is used to give an alternate way to placing points.
   *
   * @param tileX
   * @param tileY
   */
  void addFieldPoint(double tileX, double tileY);

}; // namespace xVecLib

};     // namespace xVecLib
#endif // ROBOT_H