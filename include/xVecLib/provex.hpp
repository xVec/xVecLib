/**
 * @file provex.hpp
 * @author Cavaire (cavaire3d@gmail.com)
 * @brief
 * @version 1.0.0
 * @date 2024-10-22
 *
 * @copyright Copyright (c) 2024
 *
 */
#ifndef PROVEX
#define PROVEX
#include <cstdint>
#include "universal.h"

namespace xapi
{
    std::uint32_t get_pen();
    std::uint32_t fill_rect(const std::int16_t x0, const std::int16_t y0, const std::int16_t x1, const std::int16_t y1);
    std::uint32_t set_pen(std::uint32_t color);
    std::uint32_t draw_rect(const std::int16_t x0, const std::int16_t y0, const std::int16_t x1, const std::int16_t y1);
    std::uint32_t fill_circle(const std::int16_t x, const std::int16_t y, const std::int16_t radius);
    std::uint32_t draw_circle(const std::int16_t x, const std::int16_t y, const std::int16_t radius);
    std::uint32_t draw_line(const std::int16_t x0, const std::int16_t y0, const std::int16_t x1, const std::int16_t y1);
    uint32_t millis(void);
    void delay(const uint32_t milliseconds);

}
#ifdef PRO
#include "pros/screen.hpp"
#include "pros/rtos.hpp"
/**
 * @brief  XAPI for pros namespace
 */
namespace xapi
{

    class MotorGroup : public pros::MotorGroup
    {
    public:
        using pros::MotorGroup::MotorGroup;
        std::int32_t operator=(std::int32_t voltage)
        {
            return pros::MotorGroup::operator=(voltage);
        }
        void operator+=(MotorGroup &);
    };
    class IMU : public pros::v5::Imu
    {
    public:
        using pros::Imu::Imu;
    };
    class Motor : public pros::v5::Motor
    {
        using pros::Motor::Motor;
        std::int32_t operator=(std::int32_t voltage)
        {
            return pros::Motor::operator=(voltage);
        }
    };
    class Encoder : public pros::adi::Encoder
    {
        using pros::adi::Encoder::Encoder;
    };
    class Rotation : public pros::Rotation
    {
        using pros::Rotation::Rotation;
    };
    class Mutex : public pros::Mutex
    {
        using pros::Mutex::Mutex;
    };
}
#endif
#ifdef VEX
#include "vex.h"
/**
 * @brief  XAPI for vex namespace
 * @note Needs to be finished
 */
namespace xapi
{

    class MotorGroup : public vex::motor_group
    {
    public:
        using vex::motor_group::motor_group;
    };
    class IMU : public pros::v5::Imu
    {
    public:
        using pros::Imu::Imu;
    };
    class Motor : public pros::v5::Motor
    {
        using pros::Motor::Motor;
        std::int32_t operator=(std::int32_t voltage)
        {
            return pros::Motor::operator=(voltage);
        }
    };
    class Encoder : public pros::adi::Encoder
    {
        using pros::adi::Encoder::Encoder;
    };
    class Rotation : public pros::Rotation
    {
        using pros::Rotation::Rotation;
    };
    class Mutex : public pros::Mutex
    {
        using pros::Mutex::Mutex;
    };
}
#endif

#endif